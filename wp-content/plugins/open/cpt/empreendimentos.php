<?php
//  register post type empreendimentos 

function open_register_post_type_empreendimentos(){

    $labels = array(
        'name'                  => _x( 'Empreendimentos', 'Post type general name', 'open' ),
        'singular_name'         => _x( 'empreendimento', 'Post type singular name', 'open' ),
        'menu_name'             => _x( 'Empreendimentos', 'Admin Menu text', 'open' ),
        'name_admin_bar'        => _x( 'Empreendimento', 'Adicionar nova on Toolbar', 'open' ),
        'add_new'               => __( 'Adicionar novo', 'open' ),
        'add_new_item'          => __( 'Adicionar novo empreendimento', 'open' ),
        'new_item'              => __( 'Nova empreendimento', 'open' ),
        'edit_item'             => __( 'Editar empreendimento', 'open' ),
        'view_item'             => __( 'Ver empreendimento', 'open' ),
        'all_items'             => __( 'Todas os Empreendimentos', 'open' ),
        'search_items'          => __( 'Procurar Empreendimentos', 'open' ),
        'parent_item_colon'     => __( 'Parent Empreendimentos:', 'open' ),
        'not_found'             => __( 'Nenhum empreendimento foi encontrada', 'open' ),
        'not_found_in_trash'    => __( 'Nenhum empreendimento na lixeira', 'open' ),
        'featured_image'        => _x( 'Imagem destacada do empreendimento', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'open' ),
        'set_featured_image'    => _x( 'Selecionar imagem destacada', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'open' ),
        'remove_featured_image' => _x( 'Remover imagem destacada', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'open' ),
        'use_featured_image'    => _x( 'Usar como imagem destacada', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'open' ),
        'archives'              => _x( 'Arquivos de empreendimento', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'open' ),
        'insert_into_item'      => _x( 'Inserir no empreendimento', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'open' ),
        'uploaded_to_this_item' => _x( 'Upload no empreendimento', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'open' ),
        'filter_items_list'     => _x( 'Filtrar lista de Empreendimentos', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'open' ),
        'items_list_navigation' => _x( 'Empreendimentos list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'open' ),
        'items_list'            => _x( 'Empreendimentos list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'open' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'empreendimentos' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_icon'          => 'dashicons-bank',
        'menu_position'      => null,
        'supports'           => array( 'title', 'author', 'revisions', 'thumbnail' ),
    );
 
    register_post_type( 'empreendimentos', $args );

}
add_action ('init', 'open_register_post_type_empreendimentos');


// Let us create Taxonomy for Custom Post Type
add_action( 'init', 'open_create_empreendimentos_custom_taxonomy_estado', 0 );
 
//create a custom taxonomy 
function open_create_empreendimentos_custom_taxonomy_estado() {
 
    $labels = array(
        'name' => _x( 'Estados', 'taxonomy general name' ),
        'singular_name' => _x( 'Estado', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar estados' ),
        'all_items' => __( 'Todos os estados' ),
        'parent_item' => __( 'Parent Estado' ),
        'parent_item_colon' => __( 'Parent Estado:' ),
        'edit_item' => __( 'Editar Estado' ), 
        'update_item' => __( 'Atualizar Estado' ),
        'add_new_item' => __( 'Adicionar nova Estado' ),
        'new_item_name' => __( 'Novo nome para Estado' ),
        'menu_name' => __( 'Estados' ),
    ); 	
 
    register_taxonomy('estado',array('empreendimentos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'show_in_rest' => true,
        'rewrite' => array( 'slug' => 'estados' ),
    ));
}

// Let us create Taxonomy for Custom Post Type
add_action( 'init', 'open_create_empreendimentos_custom_taxonomy_cidade', 0 );
 
//create a custom taxonomy 
function open_create_empreendimentos_custom_taxonomy_cidade() {
 
    $labels = array(
        'name' => _x( 'Cidade', 'taxonomy general name' ),
        'singular_name' => _x( 'Cidade', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar cidades' ),
        'all_items' => __( 'Todas as cidades' ),
        'parent_item' => __( 'Parent cidades' ),
        'parent_item_colon' => __( 'Parent cidades:' ),
        'edit_item' => __( 'Editar cidade' ), 
        'update_item' => __( 'Atualizar cidade' ),
        'add_new_item' => __( 'Adicionar nova Cidade' ),
        'new_item_name' => __( 'Novo nome para cidade' ),
        'menu_name' => __( 'Cidades' ),
    ); 	
 
    register_taxonomy('cidade',array('empreendimentos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'show_in_rest' => true,
        'rewrite' => array( 'slug' => 'cidades' ),
    ));
}

// Let us create Taxonomy for Custom Post Type
add_action( 'init', 'open_create_empreendimentos_custom_taxonomy_status', 0 );
 
//create a custom taxonomy 
function open_create_empreendimentos_custom_taxonomy_status() {
 
    $labels = array(
        'name' => _x( 'Status da Obra', 'taxonomy general name' ),
        'singular_name' => _x( 'Status da obra', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar Status da obra' ),
        'all_items' => __( 'Todas as Status da obra' ),
        'parent_item' => __( 'Parent Status da obra' ),
        'parent_item_colon' => __( 'Parent Status da obra:' ),
        'edit_item' => __( 'Editar status da obra' ), 
        'update_item' => __( 'Atualizar status da obra' ),
        'add_new_item' => __( 'Adicionar nova status da obra' ),
        'new_item_name' => __( 'Novo nome para status da obra' ),
        'menu_name' => __( 'Status da obra' ),
    ); 	
 
    register_taxonomy('status',array('empreendimentos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'show_in_rest' => true,
        'rewrite' => array( 'slug' => 'status-da-obra' ),
    ));
}


// Let us create Taxonomy for Custom Post Type
add_action( 'init', 'open_create_empreendimentos_custom_taxonomy_bairro', 0 );
 
//create a custom taxonomy 
function open_create_empreendimentos_custom_taxonomy_bairro() {
 
    $labels = array(
        'name' => _x( 'Bairro', 'taxonomy general name' ),
        'singular_name' => _x( 'Bairro', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar Bairro' ),
        'all_items' => __( 'Todas as Bairro' ),
        'parent_item' => __( 'Parent Bairro' ),
        'parent_item_colon' => __( 'Parent Bairro:' ),
        'edit_item' => __( 'Editar Bairro' ), 
        'update_item' => __( 'Atualizar Bairro' ),
        'add_new_item' => __( 'Adicionar nova Bairro' ),
        'new_item_name' => __( 'Novo nome para Bairro' ),
        'menu_name' => __( 'Bairro' ),
    ); 	
 
    register_taxonomy('bairro',array('empreendimentos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'show_in_rest' => true,
        'rewrite' => array( 'slug' => 'bairro' ),
    ));
}


 