<?php
/**
 * Plugin Name: Agência Open 
 * Plugin URI: https://www.agenciaopen.com
 * Description: Plugin criado pela Agência Open
 * Version: 0.1
 * Author: mateus lara
 * Author URI:  https://www.agenciaopen.com
 * License: GPL2

 */

include( plugin_dir_path( __FILE__ ) . 'cpt/index.php');

include( plugin_dir_path( __FILE__ ) . 'admin/index.php');

