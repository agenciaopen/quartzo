<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>
	
	Encontrados <?php echo $query->found_posts; ?> Resultados<br />
	Página <?php echo $query->query['paged']; ?> de <?php echo $query->max_num_pages; ?><br />
	
	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	<div class="row mt-5">
	
	<?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>
				<div class="col-md-4  mb-4 text-center" id="dicas">
					<div class="col-md-12 card">
					
			<?php 
                 
                 if(wp_is_mobile()):
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'full'); 
                else:
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
                endif;
                ?>
				<img src='<?php echo $featured_img_url;?>' class='img-fluid' alt='' title='' loading='lazy'>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<?php 
$term_list = get_the_terms($post->ID, 'linha');
$types ='';
if (!empty($term_list)) {

foreach($term_list as $term_single) {
     $types .= ucfirst($term_single->name).', ';
}
$typesz = rtrim($types, ', ');?>
<p class="text-center"><?php echo  $typesz; ?></p>
 <?php }
?>

			<p class="d-none"><?php the_category(); ?></p>
			<p class="d-none"><?php the_tags(); ?></p>
			<p class="d-none"><small><?php the_date(); ?></small></p>
			<a href="<?php the_permalink(); ?>">
				<div class="btn btn_first">
					Saiba mais
				</div>
			</a>
		</div>
		</div>

		<?php
	}
	?>
	Página <?php echo $query->query['paged']; ?> de <?php echo $query->max_num_pages; ?><br />
	</div>

	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	<?php
}
else
{
	echo "Sem resultados";
}
?>

