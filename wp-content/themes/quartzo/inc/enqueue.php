<?php



/**

 * enqueue global styles

 */
//Remove Gutenberg Block Library CSS from loading on the frontend
function smartwp_remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
} 
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );
function wpdocs_theme_name_scripts()

{
wp_enqueue_style('fontss-theme', get_template_directory_uri() . '/css/fonts.css', array(), rand(111, 9999), 'all');
if ( wp_is_mobile() ) : 
    /* Display and echo mobile specific stuff here */
 else : 
   wp_enqueue_style('aos-theme', get_template_directory_uri() . '/css/aos.css', array(), rand(111, 9999), 'all');
endif;
	

	wp_enqueue_style('slick-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css', array(), rand(111, 9999), 'all');

	wp_enqueue_style('slick',  get_template_directory_uri() . '/css/slick.min.css', array(), rand(111, 9999), 'all');
	
	if (! is_page_template('template-page-home.php')) :
		wp_enqueue_style('blueimp-img', 'https://cdnjs.cloudflare.com/ajax/libs/blueimp-gallery/3.3.0/css/blueimp-gallery.min.css', array(), rand(111, 9999), 'all');
		wp_enqueue_style('vid-popup', 'https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css', array(), rand(111, 9999), 'all');
		wp_enqueue_style('fancybox', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css', array(), rand(111, 9999), 'all');
		wp_enqueue_style('hover', 'https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.1.0/css/hover-min.css', array(), rand(111, 9999), 'all');

	endif;
	wp_enqueue_style('global', get_template_directory_uri() . '/css/style.min.css', array(), rand(111, 9999), 'all');

	wp_enqueue_style('fa', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), rand(111, 9999), 'all');


	wp_deregister_script('jquery');

	wp_register_script('jquery', get_template_directory_uri() . '/js/jquery2.min.js', false, false, true);

	wp_enqueue_script('bs4', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'),  '', 'all');

	wp_enqueue_script('slick-js',  get_template_directory_uri() . '/js/slick.js', array('jquery'),  rand(111, 9999), 'all');
	if (! is_page_template('template-page-home.php')) :
		wp_enqueue_script('blueimp-js', 'https://cdnjs.cloudflare.com/ajax/libs/blueimp-gallery/3.3.0/js/jquery.blueimp-gallery.min.js', array('jquery'),  rand(111, 9999), 'all');

		wp_enqueue_script('vid-popup-js', 'https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js', array('jquery'),  rand(111, 9999), 'all');

		wp_enqueue_script('fancybox-js', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js', array('jquery'),  rand(111, 9999), 'all');

		wp_enqueue_script('interna-pagejs', get_template_directory_uri() . '/js/empreendimentoInterna.js', array('jquery'),  '', 'all');

	endif;


	wp_enqueue_script('interna-page', get_template_directory_uri() . '/js/banner.js', array('jquery'),  '', 'all');



	wp_enqueue_script('amask', get_template_directory_uri() . '/js/jquery.mask.min.js', array('jquery'));

	wp_enqueue_script( 'MASK-h2-js', get_template_directory_uri() . '/js/mask.js', array('jquery'),  rand(111,9999), 'all' );




		if (is_singular('empreendimentos')): 

		else:

			wp_enqueue_script('sticky', get_template_directory_uri() . '/js/sticky.js', array('jquery'),  '', 'all');

		endif;



	if (is_page_template('template-page-about.php')) :

		wp_enqueue_script('inst-page', get_template_directory_uri() . '/js/institucional.js', array('jquery'),  '', 'all');



	endif;

	if (is_page_template('template-page-empreendimentos.php')) :

		wp_enqueue_script('inst-page', get_template_directory_uri() . '/js/empreendimentos.js', array('jquery'),  '', 'all');

	endif;

	if (is_page_template('template-page-empreendimentosbusca.php')) :

		wp_enqueue_script('inst-page', get_template_directory_uri() . '/js/empreendimentos.js', array('jquery'),  '', 'all');

	endif;

	if (is_page_template('template-page-home.php')) :

		wp_enqueue_script('home-page', get_template_directory_uri() . '/js/home.js', array('jquery'),  '', 'all');

	endif;

	if (is_page_template('template-page-mcmv.php')) :

		wp_enqueue_script('inst-page', get_template_directory_uri() . '/js/mcmv.js', array('jquery'),  '', 'all');

	endif;



	wp_enqueue_script('global-js', get_template_directory_uri() . '/js/global.js', array('jquery'),  rand(111, 9999), 'all');

}

add_action('wp_enqueue_scripts', 'wpdocs_theme_name_scripts');



add_action('wp_head', 'cli_add_bar_class', 9);

function cli_add_bar_class() {

	if (class_exists('Cookie_Law_Info_Public')) {



		wp_enqueue_script('cookies-page', get_template_directory_uri() . '/js/cookies.js', array('jquery'),  '', 'all');





    }

}