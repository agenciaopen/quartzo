$(window).scroll(function(){
    if ($(window).scrollTop() >= 100) {
        $('.search_container').addClass('fixed-header');
        $('.search_container div a img').addClass('visible-img');
    }
    else {
        $('.search_container').removeClass('fixed-header');
        $('.search_container div a img').removeClass('visible-img');
    }
    if ($(window).scrollTop() >= 765) {
        $('.main_first').addClass('invisible');
        $('.main_first').removeClass('visible');
        $('.fixed-bottom').removeClass('nav_down');
        $('.fixed-bottom').addClass('nav_up  ');
    }
    else {
        $('.main_first').removeClass('invisible  ');
        $('.fixed-bottom').removeClass('nav_up  ');
        $('.main_first').addClass('visible  ');
        $('.fixed-bottom').addClass('nav_down  ');    
    }
});
