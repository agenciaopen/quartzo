/////////////////////carousel
$('.carousel_empreendimentos').slick({
  rows: 2,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: false,
  dots: false,
  focusOnSelect: true,
  responsive: [
  //   {
  //     breakpoint: 9999,
  //     settings: "unslick",
  // },
    {
      breakpoint: 992,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,

      }
    },
    {
      breakpoint: 768,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,

      }
    },
    {
      breakpoint: 580,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    }
  ]
});

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  $('.carousel_empreendimentos').slick('setPosition');
})

$('.carousel_portfolio').slick({
  rows: 2,
  slidesToShow: 2,
  slidesToScroll: 1,
  arrows: false,
  dots: false,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 580,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    }
  ]
});

$('.carousel_portfolio_mobile').slick({
  rows: 2,
  slidesToShow: 2,
  slidesToScroll: 1,
  arrows: false,
  dots: false,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        rows: 2,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        rows: 2,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
      }
    },
    {
      breakpoint: 580,
      settings: {
        slidesToShow: 2,
        rows: 2,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
      }
    },
    {
      breakpoint: 380,
      settings: {
        slidesToShow: 2,
        rows: 2,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
      }
    }
  ]
});

$('.portfolio_collapse_header').on('click', function(e) {
  $('.portfolio_collapse_header').resize();
  $(".carousel_portfolio_mobile").slick("refresh");
});
