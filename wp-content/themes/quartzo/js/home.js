
$('.carousel_home').slick({
  rows: 2,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: false,
  dots: false,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 1025,
      settings: {
        rows: 2,
        arrows: false,
        dots: true,
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 992,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 580,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    }
  ]
});