
      $(allInView);
      $(window).scroll(allInView);
      function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
    
        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();
    
        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }
    function allInView() {
      if (isScrolledIntoView($("section.construcoes.background_first"))) {
        $('.counter').each(function() {
          var $this = $(this),
            countTo = $this.attr('data-count');
        
          $({
            countNum: $this.text()
          }).animate({
              countNum: countTo
            },
        
            {
              duration: 2000,
              easing: 'linear',
              step: function() {
                $this.text(commaSeparateNumber(Math.floor(this.countNum)));
              },
              complete: function() {
                $this.text(commaSeparateNumber(this.countNum));
                //alert('finished');
              }
            }
          );
        
        });
        function commaSeparateNumber(val) {
          while (/(\d+)(\d{3})/.test(val.toString())) {
            tt = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
            val = tt.replace(/,/g, '.')

          }
          return val;
        }
      }
    }
$('.carousel_parceiros').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    focusOnSelect: true,
    responsive: [
    {
        breakpoint: 992,
        settings: {
            dots: true,
            arrows: false,
            slidesToShow:1,
        }
    },
    {
      breakpoint: 768,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow:1,
      }
    },
    {
      breakpoint: 580,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow:1,
      }
    }
    ]
});
       

$('.carousel_construcoes').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    focusOnSelect: true,
    responsive: [
    {
        breakpoint: 992,
        settings: {
            dots: true,
            arrows: false,
            slidesToShow:1,
        }
    },
    {
      breakpoint: 768,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow:1,
      }
    },
    {
      breakpoint: 580,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow:1,
      }
    }
    ]
});
       


$('.carousel_missao').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  dots: false,
  focusOnSelect: true,
  responsive: [
  {
      breakpoint: 992,
      settings: {
          dots: true,
          arrows: false,
          slidesToShow:1,
      }
  },
  {
    breakpoint: 768,
    settings: {
      dots: true,
      arrows: false,
      slidesToShow:1,
    }
  },
  {
    breakpoint: 580,
    settings: {
      dots: true,
      arrows: false,
      slidesToShow: 1,
    }
  },
  {
    breakpoint: 380,
    settings: {
      dots: true,
      arrows: false,
      slidesToShow:1,
    }
  }
  ]
});




$('.carousel_linhas').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  dots: false,
  focusOnSelect: true,
  responsive: [
  {
      breakpoint: 992,
      settings: {
          dots: true,
          arrows: false,
          slidesToShow:1,
      }
  },
  {
    breakpoint: 768,
    settings: {
      dots: true,
      arrows: false,
      slidesToShow:1,
    }
  },
  {
    breakpoint: 580,
    settings: {
      dots: true,
      arrows: false,
      slidesToShow: 1,
    }
  },
  {
    breakpoint: 380,
    settings: {
      dots: true,
      arrows: false,
      slidesToShow:1,
    }
  }
  ]
});