
       var header = $("#nav_main");
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 50) {
                header.addClass("scrolled");
            } else {
                header.removeClass("scrolled");
            }
        });
        $('#bs-example-navbar-collapse-1').on('show.bs.collapse', function () {
            $(".navbar-collapse").addClass("show");
        })
          
        $('#bs-example-navbar-collapse-1').on('hide.bs.collapse', function () {
            $(".navbar-collapse").removeClass("show");
        })
       
        /*==============================================================
        navbar fixed top
        ==============================================================*/
        // Hide Header on on scroll down
        var didScroll;
        var lastScrollTop = 0;
        var delta = 100;
        var navbarHeight = $('#nav_main').outerHeight();

        $(window).scroll(function (event) {
            didScroll = true;
        });

        setInterval(function () {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        function hasScrolled() {
            var st = $(this).scrollTop();

            // Make sure they scroll more than delta
            if (Math.abs(lastScrollTop - st) <= delta)
                return;

            // If they scrolled down and are past the navbar, add class .nav-up.
            // This is necessary so you never see what is "behind" the navbar.
            if (st > lastScrollTop && st > navbarHeight) {
                // Scroll Down
                $('.navbar').removeClass('nav-down').addClass('nav-up');
            } else {
                // Scroll Up
                if (st + $(window).height() < $(document).height()) {
                    $('.navbar').removeClass('nav-up').addClass('nav-down');
                }
            }

            lastScrollTop = st;
        }

// var behavior = function (val) {
//     return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
// },
//     options = {
//         onKeyPress: function (val, e, field, options) {
//             field.mask(behavior.apply({}, arguments), options);
//         }
//     };

// $('input.tel, input.wpcf7-tel').mask(behavior, options);


/*
accordion
*/
$('#accordion .collapse').on('shown.bs.collapse', function (e) {
    $(this).prev().addClass('active');
    var $card = $(this).closest('.card');
    var $open = $($(this).data('parent')).find('.collapse.show');
    var additionalOffset = 60;
    if($card.prevAll().filter($open.closest('.card')).length !== 0)
    {
          additionalOffset =  $open.height();
    }
    $('html,body').animate({
      scrollTop: $card.offset().top - additionalOffset
    }, 500);
});
$('#accordion .collapse').on('hidden.bs.collapse', function () {
    $(this).prev().removeClass('active');
});

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
});

var activeTab = localStorage.getItem('activeTab');
if(activeTab){
    $('.nav-tabs a[href="' + activeTab + '"]').tab('show');
}


// wire up shown event
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    console.log("tab shown...");
});

// read hash from page load and change tab
var hash = document.location.hash;
var prefix = "tab_";
if (hash) {
    $('.nav-pills a[href="'+hash.replace(prefix,"")+'"]').tab('show');
} 

$(document).on('click', '#search-filter-form-140 .sf-field-taxonomy-estado select', function() {
    // $(this).find('option').get(0).remove();

});
// $(document).on('click', '#search-filter-form-140 .sf-field-taxonomy-cidade select', function() {
//     $(this).find('option').get(0).remove();
// });
// $(document).on('click', '#search-filter-form-140 .sf-field-taxonomy-bairro select', function() {
//     $(this).find('option').get(0).remove();
// });
// $(document).on('click', '#search-filter-form-140 .sf-field-taxonomy-status select', function() {
//     $(this).find('option').get(0).remove();
// });

$('#search-filter-form-140 .sf-field-taxonomy-estado select').on('change', function() {
 
    $("<option>", { text: 'Estado', value: ''}).prependTo("#search-filter-form-140 .sf-field-taxonomy-estado select");

});
$("<option>", { text: 'Estado', value: '', selected: true}).prependTo("#search-filter-form-140 .sf-field-taxonomy-estado select");
$("#search-filter-form-140 .sf-field-taxonomy-estado select option:nth-child(1)").css("display", "none");

$('#search-filter-form-140 .sf-field-taxonomy-cidade select').on('change', function() {
    $("<option>", { text: 'Cidade', value: ''}).prependTo("#search-filter-form-140 .sf-field-taxonomy-cidade select");
});
$("<option>", { text: 'Cidade', value: '', selected: true}).prependTo("#search-filter-form-140 .sf-field-taxonomy-cidade select");
$("#search-filter-form-140 .sf-field-taxonomy-cidade select option:nth-child(1)").css("display", "none");

$('#search-filter-form-140 .sf-field-taxonomy-bairro select').on('change', function() {
    $("<option>", { text: 'Bairro', value: ''}).prependTo("#search-filter-form-140 .sf-field-taxonomy-bairro select");
});
$("<option>", { text: 'Bairro', value: '', selected: true}).prependTo("#search-filter-form-140 .sf-field-taxonomy-bairro select");
$("#search-filter-form-140 .sf-field-taxonomy-bairro select option:nth-child(1)").css("display", "none");

$('#search-filter-form-140 .sf-field-taxonomy-status select').on('change', function() {
    $("<option>", { text: 'Status da Obra', value: ''}).prependTo("#search-filter-form-140 .sf-field-taxonomy-status select");
});
$("<option>", { text: 'Status da Obra', value: '', selected: true}).prependTo("#search-filter-form-140 .sf-field-taxonomy-status select");
$("#search-filter-form-140 .sf-field-taxonomy-status select option:nth-child(1)").css("display", "none");

$('.fgts input').mask('#.##0,00', {reverse: true});

