$('.carousel_banner').slick({
  rows: 1,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  dots: true,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 580,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        rows: 1,
        arrows: false,
        dots: true,
        slidesToShow: 1,
      }
    }
  ]
});