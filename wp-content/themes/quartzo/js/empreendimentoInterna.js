/////////////// carousel 
$('.carousel_ape').slick({
  slidesToShow: 5,
  rows: 1,
  slidesToScroll: 1,
  dots: false,
  focusOnSelect: true,
  gutters:0,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
        adaptiveHeight: true,

      }
    },
    {
      breakpoint: 580,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    }
  ]
});

$(document).ready(function(){
  $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  fade: true,
  asNavFor: '.carousel_planta'
});

$('.carousel_planta').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  arrows:false,
  centerMode: true,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 580,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
      }
    }
  ]
});
	
});



$('.carousel_status').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  dots: false,
  focusOnSelect: true,
  arrows: true,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 580,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
      }
    }
  ]
});



$('.btn_single').on('click', function(e) {
  $('.btn_single').resize();
  $(".carousel_planta_mobile").slick("refresh");
});
$('.carousel_planta_mobile').slick({
  slidesToShow: 1,
  rows: 1,
  slidesToScroll: 1,
  dots: false,
  focusOnSelect: true,
  gutters:0,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    },
    {
      breakpoint: 580,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    }
  ]
});


$('.btn_single').on('click', function(e) {
  $('.btn_single').resize();
  $(".carousel_status_mobile").slick("refresh");
});

$('.carousel_status_mobile').slick({
  slidesToShow: 1,
  rows: 1,
  slidesToScroll: 1,
  dots: false,
  focusOnSelect: true,
  gutters:0,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    },
    {
      breakpoint: 580,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        dots: true,
        arrows: false,
        slidesToShow: 1,
        rows: 1,
      }
    }
  ]
});
var position = $('.menuinterna').offset().top;


$(window).scroll(function(){
  console.log(position);
  if ($(window).scrollTop() >= position) {
      $('.menuinterna').addClass('fixed-top  w-100 h-auto ');

  }
  else {
    $('.menuinterna').removeClass('fixed-top  w-100 h-auto');

  }
  
});
