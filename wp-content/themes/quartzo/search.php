<?php
	get_header();
?>
<?php if ( have_posts() ) : ?>
	<section id="search_page" class="pt-0">
		<div class="container h-100">
			<div class="row h-100 h-justify-content-center align-items-center">
				<div class="col-md-12 row m-0 p-0" id="list_posts">
						<div class="col-md-12">
							<header class="page-header col-md-12 mb-5">
								<h1 class="page-title">
									<?php global $wp_query; ?>
									(<?php echo $wp_query->found_posts;?>)
									<?php printf( esc_html__( 'Resultados de busca para: %s', 'understrap' ), '<span>' . get_search_query() . '</span>'); ?>
								</h1>
							</header><!-- .page-header -->
						</div>
						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'loop-templates/content', 'search' ); ?>
						<?php endwhile; ?>
				</div><!--/.-->

				<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

			</div><!--/row.-->
		</div><!--/.container-->
	</section><!--/.search-->
	<?php else : ?>
		<section id="search_page" class="p-0">
			<div class="container h-100">
				<div class="row h-100 h-justify-content-center align-items-center">
					<div class="col-md-12 row m-0" id="list_posts">
						<header class="page-header col-md-12">
							<h1 class="page-title">
								
								<?php global $wp_query; ?>
								(<?php echo $wp_query->found_posts;?>)
								<?php printf( esc_html__( 'Resultados de busca para: %s', 'understrap' ), '<span>' . get_search_query() . '</span>'); ?>
							</h1>
						</header><!-- .page-header -->
						</div><!--/.-->
				</div><!--/row.-->
			</div><!--/.container-->
		</section><!--/.search-->
		<div class="container" id="no_results">
			<div class="row">
				<div class="col-md-12">
					<h2>Navegue também por:</h2>
				</div>
			</div>
		</div>
		<?php get_template_part( 'templates/global/template-part', 'list-posts' ); ?>
					
<?php endif; ?>


<?php get_footer(); ?>