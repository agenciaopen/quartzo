<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>
	
	<!-- Found <?php // echo $query->found_posts; ?> Results<br /> -->
	<div class='search-filter-results-list row m-0 '>

	<?php
	 $count = 0;
	 $animate = 100;
		while ($query->have_posts())
		{
			$query->the_post();
			
			?>
			      
                    
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="col-md-6 col-lg-4 mb-4">
                                <div class="text-center item col-md-12 p-0 mb-4"  data-aos="fade-up"
                    data-aos-delay="<?php echo $animate;?>"
                    data-aos-duration="500"
                    data-aos-mirror="true"
                    data-aos-once="true"
                    data-aos-anchor-placement="top-center">
                        <div class="card <?php if ($count == 0) : ?>purple background_seventh <?php endif;?> <?php if ($count == 1) : ?> blue background_seventh <?php endif;?><?php if ($count == 2): ?> orange background_seventh <?php endif;?><?php if($count == 3): ?> green background_seventh <?php endif;?>">
                            <div class="card-header background_first">
                                <div class="col-12 text-center">
                                    <h4 class="card_titulo"><?php the_title(''); ?></h4>
                                </div>
                            </div><!-- end card header -->
                            <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');
                                        if ( $url ) :
                                            $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');
                                        else :
                                            $url = '/wp-content/themes/quartzo/img/QUARTZO - ED. Alto Boa Vista - Torres Noturna.png';
                                        endif;
                                    ?>
                            <img class="card-img-top img-fluid img_filter" src="<?php echo $url;?>" alt="<?php the_title(''); ?>" title="<?php the_title(''); ?>">
                            <div class="card-body row m-0  justify-content-center align-items-center">
                                <div class="card_fa col-12 text-center p-0 card_img_border">
                                    <i class="fa fa-map-marker"></i>
                                    <?php the_field( 'localizacao' ); ?>
                                    <hr class="hr_card">
                                </div>
                                <div class="row col-12 align-items-center justify-content-center">
                                <?php
                        $terms = get_the_terms($post->ID, 'status');
                        $i = 0;

                            if( $terms):
                                foreach( $terms as $t ):
                        ?>
                                    <div class="col-12 col-lg-6">
                                        <div class="card_badge col-12">
                                       
                        
									<?php
									$taxonomy_prefix = 'status';
									$term_id = $t->term_id ;
									$term_id_prefixed = $taxonomy_prefix .'_'. $term_id;

									?>
									                    <?php echo $t->name;?>


                                        </div>
                                    </div>
                                    <?php
                                    $i++;

                                endforeach;
                            endif;
                        ?>
                                <?php if ($i > 1) : ?>
                                <?php else : ?>
                                        <?php if (get_field('quartos')) : ?> 
                                        <div class="col-12 col-lg-5">
                                            <div class="card_badge col-12">
                                            <?php the_field( 'quartos' ); ?>

                                            </div>
                                        </div>
                                    <?php endif; endif ;?>
                                </div>
                                <div class="row col-12 align-items-center justify-content-center my-4 animate__animated animate__bounce">
                                    <?php if ( have_rows( 'cadastro_de_itens_ficha_tecnica_' ) ) : ?>
                                        <?php while ( have_rows( 'cadastro_de_itens_ficha_tecnica_' ) ) : the_row(); ?>
                                            <?php if ( get_sub_field( 'destacar' ) == 1 ) : ?>
                                                <?php if ( get_sub_field( 'icone' ) ) : ?>
                                                    <div class="col-3">
                                                        <img src="<?php the_sub_field( 'icone' ); ?>" class="img-fluid" alt="<?php the_sub_field( 'item' ); ?>" title="<?php the_sub_field( 'item' ); ?>" />
                                                    </div>
                                                <?php endif ?>
                                            <?php else : ?>
                                                <?php // echo 'false'; ?>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    <?php else : ?>
                                        <?php // no rows found ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div><!-- end card -->
                    </div>
                </a>
                           

                    <?php                   $animate +=100;       $count++;  ?>
			
			<?php
		}
	?>
		</div>
		</div>

	</div>

<?php
}
else
{
	?>
	<div class='search-filter-results-list text-center mt-4 d-none' data-search-filter-action='infinite-scroll-end'>
		<span>Final dos resultados</span>
	</div>
	<?php
}
?>