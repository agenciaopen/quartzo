<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="missao ">
    <div class="container h-100 background_seventh ">
        <div class="row h-100 align-items-center justify-content-center m-1">
            <div class="col-lg-12 carousel_missao borda_cards_about m-3">
                <div class="text-center item">
                    <div class="card">
                        <div class="col-12 p-0 d-flex align-items-center justify-content-center">
                            <?php if ( get_field( 'imagem_missao_', $page_ID ) ) : ?>
                                <img data-aos="fade-in" src="<?php the_field( 'imagem_missao_' ); ?>"  class='img-fluid ' alt='<?php the_field( 'titulo_missao', $page_ID ); ?>' title='<?php the_field( 'titulo_missao', $page_ID ); ?>' lazy='loading'>
                            <?php else: ?>    
                                <img data-aos="fade-in" src='/wp-content/themes/quartzo/img/Grupo 1706.png' class='img-fluid ' alt='<?php the_field( 'titulo_missao', $page_ID ); ?>' title='<?php the_field( 'titulo_missao', $page_ID ); ?>' lazy='loading'>
                            <?php endif; ?>    
                        </div>
                        <h2 data-aos="fade-in-down" class="text-center"><?php the_field( 'titulo_missao', $page_ID ); ?></h2>
                        <hr class="hr_bot">
                        <p>
                            <?php the_field( 'descricao_missao', $page_ID ); ?>
                        </p>
                    </div><!-- /.card -->
                </div>
                <div class="text-center item">
                    <div class="card carousel_missao_border">
                        <div class="col-12 p-0 d-flex align-items-center justify-content-center">
                            <?php if ( get_field( 'titulo_visao_copiar', $page_ID  ) ) : ?>
                                <img data-aos="fade-in" src="<?php the_field( 'titulo_visao_copiar', $page_ID  ); ?>"  class='img-fluid ' alt='<?php the_field( 'titulo_visao', $page_ID ); ?>' title='<?php the_field( 'titulo_visao', $page_ID ); ?>' lazy='loading'>
                            <?php endif ?>
                            <img data-aos="fade-in" src='/wp-content/themes/quartzo/img/noun_street_433086.png' class='img-fluid ' alt='<?php the_field( 'titulo_visao', $page_ID ); ?>' title='<?php the_field( 'titulo_visao', $page_ID ); ?>' lazy='loading'>
                        </div>
                        <h2 data-aos="fade-in-down" class="text-center"><?php the_field( 'titulo_visao', $page_ID ); ?></h2>
                        <hr class="hr_bot">
                        <p>
                            <?php the_field( 'descricao_visao', $page_ID ); ?>
                        </p>
                    </div><!-- /.card -->
                </div>
                <div class="text-center item">
                    <div class="card">
                        <div class="col-12 p-0 d-flex align-items-center justify-content-center">
                            <?php if ( get_field( 'titulo_valores_copiar', $page_ID ) ) : ?>
                                <img data-aos="fade-in" src="<?php the_field( 'titulo_valores_copiar', $page_ID ); ?>" />
                            <?php endif ?>
                            <img data-aos="fade-in" src='/wp-content/themes/quartzo/img/Grupo 1708.png' class='img-fluid' alt='parceiros' title='parceiros' lazy='loading'>
                        </div>
                        <h2 data-aos="fade-in-down" class="text-center"><?php the_field( 'titulo_valores', $page_ID ); ?></h2>
                        <hr class="hr_bot">
                        <?php the_field( 'descricao_valores', $page_ID ); ?>
                        
                    </div><!-- /.card -->
                </div>
            </div>
        </div>
    </div>
</section><!-- /.history -->