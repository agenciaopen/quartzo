<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="institucionalMobile parceiros_colapse d-block d-lg-none pt-0">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div id="accordion" class="accordion col-10">
                <div class="card mb-0">
                    <div class="card-header collapsed background_first padding_accordion_about mt-3" data-toggle="collapse" href="#collapseOne">
                        <a class="card-title collapse_title ">
                            <?php the_field( 'titulo_institucional', $page_ID ); ?>
                        </a>
                    </div>
                    <div id="collapseOne" class="card-body collapse accordion_border mb-3" data-parent="#accordion">
                        <p class="collapse_text col-12 py-4 px-3">                    
                            <?php the_field( 'descricao_institucional', $page_ID, false, false ); ?>
                        </p>
                    </div>
                    <div class="card-header collapsed background_first padding_accordion_about mt-3" data-toggle="collapse" href="#collapseTwo">
                        <a class="card-title collapse_title ">
                            <?php the_field( 'titulo_parceiros', $page_ID ); ?>
                        </a>
                    </div>
                    <div id="collapseTwo" class="card-body collapse accordion_border mb-3" data-parent="#accordion">
                        <p class="collapse_text col-12 py-4">
                            <?php the_field( 'descricao_parceiros', $page_ID, false, false ); ?>

                        </p>
                    </div>
                    <div class="card-header collapsed background_first padding_accordion_about mt-3" data-toggle="collapse" href="#collapseThree">
                        <a class="card-title collapse_title ">
                            <?php the_field( 'titulo_historia', $page_ID ); ?>
                        </a>
                    </div>
                    <div id="collapseThree" class="card-body collapse accordion_border mb-3" data-parent="#accordion">
                        <p class="collapse_text col-12 py-4">
                            <?php the_field( 'descricao_historia', $page_ID, false, false ); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.institucional -->