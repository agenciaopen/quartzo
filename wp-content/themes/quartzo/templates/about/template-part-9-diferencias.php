<?php 
global $post;
$page_ID = $post->ID;


?>

<section class="diferenciais">
    <div class="container h-100 ">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-lg-12 text_description ">
                <div class="col-12 row m-0 justify-content-start aling-items-strech">
                    <h2 class="col-lg-12 text-center"><?php the_field( 'titulo_nossos_diferenciais', $page_ID ); ?></h2>
                    <hr class="hr_bot">
                </div><!-- /.col-12 row justify-content-start aling-items-strech -->
            </div>
            <?php if ( have_rows( 'cadastro_de_nossos_diferenciais', $page_ID  ) ) : ?>
                <?php                         $animate = 100;$count = 0; while ( have_rows( 'cadastro_de_nossos_diferenciais', $page_ID  ) ) : the_row(); ?>
                    <div class="row m-0 col-6 col-lg-3 text-center justify-content-center align-items-start item_height text-center hvr-grow" id="d<?php echo $count;?>" data-aos="fade-up"
                    data-aos-delay="<?php echo $animate;?>>"
                    data-aos-duration="500"
                    data-aos-mirror="true"
                    data-aos-once="true"
                    data-aos-anchor-placement="top-center">
                        <div class="diferenciais_box col-12 d-flex justify-content-center align-items-center text-center">
                            <?php if ( get_sub_field( 'icone' ) ) : ?>
                                <img src="<?php the_sub_field( 'icone' ); ?>" class=' img-fluid diferenciais_img' alt='<?php the_sub_field( 'descricao_nossos_diferenciais' ); ?>' title='<?php the_sub_field( 'descricao_nossos_diferenciais' ); ?>' lazy='loading'> 
                            <?php else : ?>
                                <?php if ($count == 0) : ?>
                                    <img src='/wp-content/themes/quartzo/img/noun_labor_1709344.png' class=' img-fluid diferenciais_img' lazy='loading'> 
                                <?php elseif($count == 1):?>
                                    <img src='/wp-content/themes/quartzo/img/noun_Led Lamp_1656833 (1).png' class=' img-fluid diferenciais_img' alt='1' title='1' lazy='loading'>
                                <?php elseif($count == 2):?>
                                    <img src='/wp-content/themes/quartzo/img/noun_House_1974652.png' class=' img-fluid diferenciais_img' alt='2' title='2' lazy='loading'>
                                <?php else : ?>
                                    <img src='/wp-content/themes/quartzo/img/noun_finish_2751817.png' class=' img-fluid diferenciais_img' alt='3' title='3' lazy='loading'>
                                <?php endif;?> 
                            <?php endif; ?>
                        </div>
                        <div class="col-md-12 text-center p-0 mx-auto d-block">
                            <p class="text-center col-md-12 p-0"><?php the_sub_field( 'descricao_nossos_diferenciais' ); ?></p>
                        </div>
                    </div>
                   
                <?php $count++;  $animate +=500; endwhile; ?>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
           
        </div>
    </div>
</section><!-- /.institutional -->