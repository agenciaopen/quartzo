
<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="linhas">
    <div class="container h-100 ">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-lg-12 text_description ">
                <div class="col-12 row justify-content-start aling-items-strech m-0">
                    <h2 class="col-lg-12 text-center"><?php the_field( 'titulo_nossas_linhas', $page_ID ); ?></h2>
                    <hr class="hr_bot">
                </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                <div class="col-lg-12 carousel_linhas">
                    
                    <?php if ( have_rows( 'cadastro_de_linhas', $page_ID ) ) : ?>
                        <?php $count = 0; while ( have_rows( 'cadastro_de_linhas', $page_ID ) ) : the_row(); ?>
                            <div class="text-center item" data-aos="fade-down">
                                <div class="card background_seventh">
                                    <div class="col-12 p-0 d-flex align-items-end justify-content-center linhas_size">
                                        <?php if ( get_sub_field( 'icone' ) ) : ?>
                                            <img src='<?php the_sub_field( 'icone' ); ?>' class='img-fluid morada_fix' alt='<?php the_sub_field( 'nome_da_linha' ); ?>' title='<?php the_sub_field( 'nome_da_linha' ); ?>' lazy='loading'>
                                        <?php else :?>
                                            <?php  if ($count == 0) : ?>
                                                <img src='/wp-content/themes/quartzo/img/Grupo 1831.png' class='img-fluid morada_fix static' alt='<?php the_sub_field( 'nome_da_linha' ); ?>' title='<?php the_sub_field( 'nome_da_linha' ); ?>' lazy='loading'>
                                            <?php endif; ?>
                                            <?php if ($count == 1) : ?> 
                                                <img src='/wp-content/themes/quartzo/img/Grupo 1866.png' class='img-fluid morada_fix static' alt='<?php the_sub_field( 'nome_da_linha' ); ?>' title='<?php the_sub_field( 'nome_da_linha' ); ?>' lazy='loading'>
                                            <?php endif; ?>
                                            <?php if ($count == 2) : ?>
                                                <img src='/wp-content/themes/quartzo/img/Grupo 1950.png' class='img-fluid morada_fix static' alt='<?php the_sub_field( 'nome_da_linha' ); ?>' title='<?php the_sub_field( 'nome_da_linha' ); ?>' lazy='loading'>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    <p class="text-uppercase pb-4 text-center"><?php the_sub_field( 'nome_da_linha' ); ?></p>
                                    <p class="text-center linhas_text">
                                        <?php the_sub_field( 'descricao_da_linha' ); ?>
                                    </p>
                                </div><!-- /.card -->
                            </div>
                        <?php $count++; endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>

                </div><!-- /.carousel -->
            </div>
        </div>
</section><!-- /.institutional -->


