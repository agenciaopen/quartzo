<?php
global $post;
$page_ID = $post->ID;


?>
<section class="location">
    <div class="container h-100 ">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-lg-6 text_description ">
                <div class="col-12 row justify-content-start aling-items-strech">
                    <div class="col-1 hr-left"></div>
                    <h2 class="col-11"><?php the_field('titulo_onde_estamos', $page_ID); ?></h2>
                </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                <p class="col-lg-10 p-0 mt-3">
                    <?php the_field('descricao_onde_estamos', $page_ID); ?>
                </p>
            </div><!-- /.col-lg-6 text_description -->
            <div class="col-12 col-lg-6 text-center">

                <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
                <script type="text/javascript" src="https://atayahmet.github.io/jquery.easypin/jquery.easypin.min.js"></script>

                <img src="/wp-content/themes/quartzo/img/mapa.svg" class="pin" width="540" easypin-id="demo_image_1" />

                <div style="display:none;" easypin-tpl>
                    <popover>
                        <div class="{[content]} exPopoverContainer">
                            <div class="popBg borderRadius"></div>
                            <div class="popBody" style="width: 120px;">
                                <div class="arrow-down" style="top: 231px;left: 13px;"></div>
                                <a href="{[link]}" >
                                    <p class="" style="color: #000; padding: 5px 10px; border: 1px solid red; border-radius: 5px;font-size: 10px;line-height: 13px; position: relative; left: 20px;background: #fff;">{[content]}</p>
                                </a>
                                <div class="popHeadLine"></div>
                            </div>
                        </div>
                    </popover>
                    <style>
                        .popBody a:hover {
                            text-decoration: none;
                        }

                        .marker2.element-animation.easypin-marker {
                            padding: 5px;
                        }
                    </style>
                    <marker>
                        <div class="marker2 element-animation">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 498.22 498.22" style="enable-background:new 0 0 498.22 498.22;  width:20px; height: 20px;" xml:space="preserve">
                                <g>
                                    <g>
                                        <g>
                                            <path style="fill: #fff;" d="M269.061,484.131c-3.185,8.461-11.255,14.049-20.273,14.089     c-9.028,0.029-17.147-5.501-20.39-13.913c-44.034-114.321-132.63-261.205-132.63-330.964C95.767,68.801,164.539,0,249.12,0     c84.541,0,153.333,68.801,153.333,153.343C402.462,223.307,312.557,368.579,269.061,484.131z M249.12,29.164     c-66.32,0-120.261,53.941-120.261,120.232c0,66.33,53.941,120.3,120.261,120.3c66.3,0,120.241-53.951,120.241-120.3     C369.351,83.105,315.42,29.164,249.12,29.164z" />
                                        </g>
                                    </g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                                <g>
                                </g>
                            </svg>
                        </div>
                    </marker>
                </div>




                <script type="text/javascript">
                    $(document).ready(function() {
                        let caminho_site = "http://quartzo.agenciaopen.com.br/resultado";
                        let cidade = "?_sft_cidade=";
                        let estado = "?_sft_estado=";

                        $('.pin').easypinShow({
                            data: {
                                "demo_image_1": {
                                    "0": {
                                        "content": "Belo Horizonte",
                                        "link": caminho_site + cidade + "belo-horizonte",
                                        "coords": {
                                            "lat": "356",
                                            "long": "251"
                                        }
                                    },
                                    "1": {
                                        "content": "Uberlândia",
                                        "link": caminho_site + cidade + "uberlandia",
                                        "coords": {
                                            "lat": "186",
                                            "long": "189"
                                        }
                                    },
                                    "2": {
                                        "content": "Ribeirão das Neves",
                                        "link": caminho_site + cidade + "ribeirao-das-neves",
                                        "coords": {
                                            "lat": "262",
                                            "long": "206"
                                        }
                                    },
                                    "3": {
                                        "content": "Itaúna",
                                        "link": caminho_site + cidade + "itauna",
                                        "coords": {
                                            "lat": "296",
                                            "long": "299"
                                        }
                                    },
                                    "4": {
                                        "content": "Contagem",
                                        "link": caminho_site + cidade + "contagem",
                                        "coords": {
                                            "lat": "329",
                                            "long": "281"
                                        }
                                    },
                                    "5": {
                                        "content": "Santa Luzia",
                                        "link": caminho_site + cidade + "santa-luzia",
                                        "coords": {
                                            "lat": "386",
                                            "long": "231"
                                        }
                                    },
                                    "6": {
                                        "content": "São Paulo",
                                        "link": caminho_site + estado + "sao-paulo",
                                        "coords": {
                                            "lat": "216",
                                            "long": "366"
                                        }
                                    },
                                    "7": {
                                        "content": "Barueri",
                                        "link": caminho_site + cidade + "barueri",
                                        "coords": {
                                            "lat": "180",
                                            "long": "382"
                                        }
                                    },
                                    "8": {
                                        "content": "Cosmópolis",
                                        "link": caminho_site + cidade + "cosmpolis",
                                        "coords": {
                                            "lat": "153",
                                            "long": "357"
                                        }
                                    },
                                    "9": {
                                        "content": "Piracicaba",
                                        "link": caminho_site + cidade + "piracicaba",
                                        "coords": {
                                            "lat": "126",
                                            "long": "321"
                                        }
                                    },
                                    "10": {
                                        "content": "Campinas",
                                        "link": caminho_site + cidade + "campinas",
                                        "coords": {
                                            "lat": "218",
                                            "long": "336"
                                        }
                                    },
                                    "canvas": {
                                        "src": "/wp-content/themes/quartzo/img/mapa.svg",
                                        "width": "540",
                                        "height": "467"
                                    }
                                }
                            },
                            // data: '{"demo_image_1":{{"0":{"title":"Belo Horizonte","coords":{"lat":"312.4","long":"191"}},"1":{"title":"Uberlândia","coords":{"lat":"159.4","long":"152.788"}},"2":{"title":"Ribeirão das Neves","coords":{"lat":"255.39999999999998","long":"167.788"}},"3":{"title":"Itaúna","coords":{"lat":"255.39999999999998","long":"226.788"}},"4":{"title":"Contagem","coords":{"lat":"287.4","long":"214.788"}},"5":{"title":"Santa Luzia","coords":{"lat":"361.4","long":"204.788"}},"6":{"title":"São Paulo","coords":{"lat":"217.4","long":"355.788"}},"7":{"title":"Barueri","coords":{"lat":"190.4","long":"356.788"}},"8":{"title":"Cosmópolis","coords":{"lat":"116.4","long":"324.788"}},"9":{"title":"Piracicaba","coords":{"lat":"103.4","long":"251.788"}},"canvas":{"src":"/wp-content/themes/quartzo/img/mapa.svg","width":"768","height":"661"}}}',
                            // data: '{"demo_image_1":{"0":{"title":"Galata Tower", "coords":{"lat":"506","long":"71"}},"1":{"title":"Ferries in Istanbul","description":"The city of Istanbul is at a geographic crossroads, straddling Europe and Asia Minor, and is divided by a sea lane called the Bosphorus Strait, which joins the Black Sea in the northeast and the Mediterranean Sea in the southwest. This strait has played a key role in the history of the city.","image_url":"https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Emin_Kul.jpg/250px-Emin_Kul.jpg","coords":{"lat":"372","long":"298"}},"canvas":{"src":"img/istanbul-wide-cropped.jpg","width":"1000","height":"359"}}}',

                            responsive: true,
                            variables: {
                                firstname: function(canvas_id, pin_id, data) {
                                    //console.log(canvas_id, pin_id, data);
                                    return data;
                                },
                                surname: function(canvas_id, pin_id, data) {
                                    //console.log(canvas_id, pin_id, data);
                                    return data;
                                }
                            },
                            popover: {
                                show: true,
                                animate: true
                            },
                            each: function(index, data) {
                                return data;
                            },
                            error: function(e) {
                                console.log(e);
                            },
                            success: function() {
                                console.log('başarılı');
                            }
                        });
                    });
                </script>


                </body>

                </html>
                <a href="/empreendimentos/" class="d-none">
                    <?php if (get_field('imagem_onde_estamos_para_notebook_e_desktops', $page_ID)) : ?>
                        <img src="<?php the_field('imagem_onde_estamos_para_notebook_e_desktops', $page_ID); ?>" class='img-fluid' alt='<?php the_field('titulo_onde_estamos', $page_ID); ?>' title='<?php the_field('titulo_onde_estamos', $page_ID); ?>' lazy='loading'>

                    <?php else :  ?>
                        <img src='/wp-content/themes/quartzo/img/Grupo 2060.png' class='img-fluid' alt='<?php the_field('titulo_onde_estamos', $page_ID); ?>' title='<?php the_field('titulo_onde_estamos', $page_ID); ?>' lazy='loading'>
                    <?php endif; ?>
                    <?php if (get_field('imagem_onde_estamos_dispositivos_moveis', $page_ID)) : ?>
                        <img src="<?php the_field('imagem_onde_estamos_dispositivos_moveis', $page_ID); ?>" class="d-none" />
                    <?php endif ?>
                </a>
            </div>
            <div class="col-12 d-inline d-lg-none">
                <?php if (have_rows('lista_de_cidades_onde_estamos', $page_ID)) : ?>
                    <ul class="lista_cidades">
                        <?php while (have_rows('lista_de_cidades_onde_estamos', $page_ID)) : the_row(); ?>
                            <li class="item_cidades">
                                <a class="text-decoration-none" href="">
                                    <?php the_sub_field('cidade'); ?>
                                </a>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php else : ?>
                    <?php // no rows found 
                    ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section><!-- /.institutional -->