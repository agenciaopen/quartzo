<?php 

global $post;

$page_ID = $post->ID;





?>

<section class="institutional">

    <div class="container h-100 d-none d-lg-block ">

        <div class="row h-100 align-items-start  justify-content-start">

            <div class="d-none d-lg-inline col-lg-6 text_description ">
              
                <div class="col-12 row justify-content-start aling-items-strech">

                    <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->

                    <div class=" col-lg-1 hr-left"></div>

                    <h1 class="col-lg-11"><?php the_field( 'titulo_institucional', $page_ID ); ?></h1>

                </div><!-- /.col-12 row justify-content-start aling-items-strech -->

                <p class="mt-3" >

                    <?php the_field( 'descricao_institucional', $page_ID ); ?>

                </p>

            </div><!-- /.col-lg-6 text_description -->

            <div class="col-lg-6 text-center">

                <?php if ( get_field( 'imagem_institucional',  $page_ID ) ) : ?>

                    <img src="<?php the_field( 'imagem_institucional' ); ?>"  class='img-fluid' alt='institutional' title='institutional' lazy='loading'> />

                <?php else: ?>

                    <img src='/wp-content/themes/quartzo/img/shutterstock_1457837645.png' class='img-fluid' alt='institutional' title='institutional' lazy='loading'>

                <?php endif; ?>

            </div>

        </div>

    </div>

    <div class="container-fluid h-100 d-block d-lg-none p-0 m-0">

        <div class="text-center">

            <?php if ( get_field( 'imagem_institucional',  $page_ID ) ) : ?>

                <img src="<?php the_field( 'imagem_institucional' ); ?>"  class='img-fluid' alt='institutional' title='institutional' lazy='loading'> />

            <?php else: ?>

                <img src='/wp-content/themes/quartzo/img/shutterstock_1457837645.png' class='img-fluid' alt='institutional' title='institutional' lazy='loading'>

            <?php endif; ?>

        </div>

    </div>

</section><!-- /.institutional -->