<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="parceiros d-none d-lg-block pb-0">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-lg-10 text-center">
                <h2 class="text-center"><?php the_field( 'titulo_parceiros', $page_ID ); ?></h2>
                <hr class="hr_bot">
            </div>
            <div class="col-lg-6 text-center">
                <p class="text-center" >
                    <?php the_field( 'descricao_parceiros', $page_ID ); ?>
                </p>
            </div>
        </div><!-- fim row textos -->
        <div class="col-12 carousel_parceiros">
            <?php if ( have_rows( 'cadastro_de_parceiros' ) ) : ?>
                <?php while ( have_rows( 'cadastro_de_parceiros' ) ) : the_row(); ?>
                    <div class="text-center item text-center">
                        <?php if ( get_sub_field( 'logo_parceiro' ) ) : ?>
                            <img data-aos="fade-in-up" src="<?php the_sub_field( 'logo_parceiro' ); ?>" class="img-fluid" alt="<?php the_sub_field( 'nome_parceiros' ); ?>" title="<?php the_sub_field( 'nome_parceiros' ); ?>" />
                        <?php endif ?>
                    </div>
                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
            
        </div>
    </div>
</section><!-- /.history -->