<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="sustentavel d-none d-lg-block">
    <div class="container h-100 ">
        <div class="row h-100 align-items-start justify-content-center">
            <div class="d-none d-lg-inline col-lg-6 text_description ">
                <div class="col-12 row justify-content-start aling-items-strech">
                    <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->
                    <div class=" col-lg-1 hr-left"></div>
                    <h2 class="col-lg-11"><?php the_field( 'titulo_empreendimentos_sustentaveis', $page_ID); ?></h2>
                </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                <p class="mt-3" >
                    <?php the_field( 'descricao_empreendimentos_sustentaveis',  $page_ID ); ?>
                </p>
            </div><!-- /.col-lg-6 text_description -->
            <div class="col-12 col-lg-6 text-center" data-aos="zoom-in-up">
                
                <?php if ( get_field( 'imagem_empreendimentos_sustentaveis', $page_ID ) ) : ?>
                    <img src="<?php the_field( 'imagem_empreendimentos_sustentaveis', $page_ID ); ?>" class=' hvr-grow img-fluid' alt='<?php the_field( 'titulo_empreendimentos_sustentaveis', $page_ID); ?>' title='<?php the_field( 'titulo_empreendimentos_sustentaveis', $page_ID); ?>' lazy='loading'>
                <?php else : ?>
                    <img src='/wp-content/themes/quartzo/img/Grupo 2063.png' class=' hvr-grow img-fluid' alt='<?php the_field( 'titulo_empreendimentos_sustentaveis', $page_ID); ?>' title='<?php the_field( 'titulo_empreendimentos_sustentaveis', $page_ID); ?>' lazy='loading'>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section><!-- /.institutional -->