<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="sustentavel_colapse d-block d-lg-none">
    <div class="container h-100 ">
        <div class="row h-100 align-items-center justify-content-center">
            <div id="accordion" class="accordion col-10">
                <div class="card mb-0">
                    <div class="card-header collapsed background_first padding_accordion_about mt-3" data-toggle="collapse" href="#collapseFour">
                        <a class="card-title collapse_title">
                            <?php the_field( 'titulo_empreendimentos_sustentaveis', $page_ID); ?>
                        </a>
                    </div>
                    <div id="collapseFour" class="card-body collapse accordion_border mb-3" data-parent="#accordion">
                        <p class="collapse_text col-12 py-4">                    
                            <?php the_field( 'descricao_empreendimentos_sustentaveis',  $page_ID , false , false ); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.institutional -->