<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="construcoes background_first">
    <div class="container h-100 ">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-lg-12 carousel_construcoes">
                <?php if ( have_rows( 'cadastro_de_historia_em_numeros', $page_ID ) ) : ?>
                    <?php $count = 0; $animate = 100; while ( have_rows( 'cadastro_de_historia_em_numeros', $page_ID ) ) : the_row(); ?>
                        <div class="d-flex flex-column justify-content-start align-items-center text-center item <?php if ($count == 1) : ?>  carousel_construcoes_border <?php endif;?>">   
                            <?php if ( get_sub_field( 'icone' ) ) : ?>
                                <div class="col-12  p-0 pt-5 d-flex align-items-center justify-content-center">
                                    <img src="<?php the_sub_field( 'icone' ); ?>" class='img-fluid mx-auto' alt='parceiros' title='parceiros' lazy='loading'>
                                </div>
                                <?php else : ?>
                                <?php if ($count == 0) : ?>
                                    <img data-aos="fade-in-down"
                    data-aos-delay="<?php echo $animate;?>"
                    data-aos-duration="100"
                    data-aos-mirror="true"
                    data-aos-once="true"
                    data-aos-anchor-placement="top-center" src='/wp-content/themes/quartzo/img/noun_bulding_3131906 (1).png' class='img-fluid mx-auto' alt='parceiros' title='parceiros' lazy='loading'>
                                    <h3 data-aos="fade-in" class="number text-white counter" data-count="10">0</h3>
                                    <p data-aos="fade-in-up" class="text-white">Empreendimentos entregues</p>
                                <?php elseif ($count == 1) : ?>
                                    <img data-aos="fade-in-down"
                    data-aos-delay="<?php echo $animate;?>"
                    data-aos-duration="100"
                    data-aos-mirror="true"
                    data-aos-once="true"
                    data-aos-anchor-placement="top-center" src='/wp-content/themes/quartzo/img/noun_Construction_3338603.png' class='img-fluid mx-auto' alt='parceiros' title='parceiros' lazy='loading'>
                                    <h3 data-aos="fade-in" class="number text-white counter" data-count="13">0</h3>
                                    <p data-aos="fade-in-up" class="text-white">Empreendimentos desenvolvidos</p>
                                <?php else : ?>
                                    <img data-aos="fade-in-down"
                    data-aos-delay="<?php echo $animate;?>"
                    data-aos-duration="100"
                    data-aos-mirror="true"
                    data-aos-once="true"
                    data-aos-anchor-placement="top-center" src='/wp-content/themes/quartzo/img/noun_Construction_2173589.png' class='img-fluid mx-auto' alt='parceiros' title='parceiros' lazy='loading'>
                                    <div class="d-flex flex-row" >
                                        <p data-aos="fade-in-up" class="text-white mr-2">+ de </p>
                                        <h3 data-aos="fade-in" class="number text-white counter" data-count="100000">0</h3>
                                    </div>
                                    <p data-aos="fade-in-up" class="text-white">m<sup>2</sup> construidos</p>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php //the_sub_field( 'numero' ); ?>
                            <?php //the_sub_field( 'descricao' ); ?>
                        </div>
                        <?php $count++; $animate +=400; endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
                
            </div>
        </div>
    </div>
</section><!-- /.history -->