<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="history d-none d-lg-block">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-lg-10 " data-aos="fade-in"> 
                <h2 class="text-center"><?php the_field( 'titulo_historia', $page_ID ); ?></h2>
                <hr class="hr_bot">
            </div>
            <div class="col-lg-8 text-center" data-aos="fade-in">
                <p class="text-center">
                    <?php the_field( 'descricao_historia', $page_ID ); ?>
                </p>
            </div>
        </div>
    </div>
</section><!-- /.history -->