<section class="banner_section_prov pt-lg-0" id="">
    <div class="container-fluid h-100 banner_container carousel_banner h-100 mb-0 p-0">
        <div class="row justify-content-start align-items-center m-0 item mb-0">
            <div class="col-12 banner_content_prov">
                <div class="col-12 col-lg-6 p-0">
                    <h2 class="text-white">
                        Entre em contato
                    </h2>
                </div>
            </div>
        </div>
    </div><!-- end container -->
    <div class="container-fluid h100 background_seventh banner_nav_prov">
        <div class="d-none d-lg-flex row m-0 col-12 p-0 justify-content-center align-items-center">
            <col-4 class=" pr-3">
                <a href="" class=" hvr-pulse-grow row p-3 w-100 h-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/vibrate.png' class='img-fluid' alt='' title='' lazy='loading'>
                    <p class="pl-3">Ligamos pra você</p>
                </a>
            </col-4>
            <col-4 class=" pr-3">
                <a href="" class=" hvr-pulse-grow row p-3  w-100 h-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/phone (1).png' class='img-fluid' alt='' title='' lazy='loading'>
                    <p class="pl-3">Contato vendas</p>
                </a>
            </col-4>
            <col-4 class=" pr-3">
                <a href="" class=" hvr-pulse-grow row p-3  w-100 h-100 m-0 justify-content-center align-items-center">
                    <img class="chat_black" src='/wp-content/themes/quartzo/img/chat.png' class='img-fluid' alt='' title='' lazy='loading'>
                    <p class="pl-3">Chat online</p>
                </a>
            </col-4>
            <div class="whatsapp_btn">
                <a href="https://wa.me/?text=Olá">
                    <i class="fa fa-whatsapp"></i>
                </a>
            </div>
        </div>
        <div class="d-lg-none row m-0 col-12 p-0 justify-content-center align-items-center">
            <div class="p-0">
                <a href="" class=" hvr-pulse-grow row p-3 w-100 h-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/vibrate.png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <div class="p-0">
                <a href="" class=" hvr-pulse-grow row p-3  w-100 h-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/phone (1).png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <div class="p-0">
                <a href="" class=" hvr-pulse-grow row p-3  w-100 h-100 m-0 justify-content-center align-items-center">
                    <img class="chat_black"src='/wp-content/themes/quartzo/img/chat.png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <div class="whatsapp_btn m-0 p-0">
                <a href="https://wa.me/?text=Olá">
                    <i class="fa fa-whatsapp"></i>
                </a>
            </div>
        </div>
    </div><!-- end container -->
</section>