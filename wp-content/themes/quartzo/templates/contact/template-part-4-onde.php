<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="onde">
    <div class="container h-100">
        <div class="row h-100 align-items-start justify-content-center">
            <div class="col-lg-10 text-center">
                <h2 class="text-center">Onde estamos</h2>
                <hr class="hr_bot">
            </div>
            <div class="d-flex justify-content-center col-12">
                <div class="column col-lg-4 justify-content-center text-center m-0">
                    <p class=" titulo_onde text-center">
                        Sede Adiministrativa
                        
                    </p>
                    <p class=" text-center">
                       <?php the_field('sede_administrativa_endereco', $page_ID);?>
                    </p>
                    <hr class="d-lg-none hr_onde">
                </div>
            </div>
            <div class="col-12 mt-lg-4 mb-lg-3" id="plantao">
                <p class="text-center titulo_onde">
                    Plantões de venda
                </p>
            </div>
            <?php if ( have_rows( 'cadastro_de_plantoes_de_venda', $page_ID ) ) : ?>
                <?php while ( have_rows( 'cadastro_de_plantoes_de_venda', $page_ID ) ) : the_row(); ?>
                    <div class="row col-12 col-lg-4 text-center">
                        <p class="col-12 text-center">
                            <?php the_sub_field( 'local' ); ?>
                        </p>
                        <p class="col-12 text-center">
                            <?php the_sub_field( 'endereco' ); ?>
                        </p>
                        <p class="col-12 text-center">
                            Telefone:                     <?php the_sub_field( 'telefone' ); ?>

                        </p>
                        <hr class="d-lg-none hr_onde">
                    </div>
                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
           
        </div><!-- fim row textos -->
    </div>
</section><!-- /.history -->