<section class="telefone background_seventh">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="row col-12 col-lg-4 m-0 p-2 pr-4 justify-content-center">
                <img class="align-self-center mr-3" src="/wp-content/themes/quartzo/img/Grupo 2068.png" alt="font awesome">
                <div class="media-body">
                    <p class="mt-0 font-weight-bold"><?php the_field( 'titulo_ja_sou_cliente' ); ?>
</p>
                    <p class="client_text1"><?php the_field( 'subtitulo_ja_sou_cliente' ); ?>
</p>
                    <p>
                    <?php the_field( 'telefone_de_contato_ja_sou_cliente' ); ?>     
                    </p>
                </div>
            </div>
            <div class="row col-12 col-lg-4 m-0 p-2">
                <div class="align-self-center mr-3 wapp d-flex justify-content-center align-items-center">
                    <div class='wapp_img' ></div>
                </div>
                <div class="media-body">
                    <p class="mt-0 font-weight-bold"><?php the_field( 'titulo_whatsapp' ); ?></p>
                    <p class="client_text font-weight-bold">Fale com a gente<br>por WhatsApp</p>
                    <p>
                    <?php
		$phone = get_field('telefone_whatsapp');
		$phone = preg_replace('/\D+/', '', $phone);
		?>
		<a href="https://api.whatsapp.com/send?phone=55<?php echo $phone; ?>?text=Ola" rel="external" target="_blank" style="    color: #373737; text-decoration: none;">
                        <?php the_field( 'telefone_whatsapp' ); ?></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.telefone -->