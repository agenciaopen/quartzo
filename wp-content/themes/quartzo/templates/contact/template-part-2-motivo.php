<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="motivo d-none d-lg-flex">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-lg-12 text_description">
                <h4 class="text-left"><strong><?php the_field( 'titulo_motivo_do_contato', $page_ID ); ?></strong></h4>
            </div><!-- /.col-lg-12 text_description -->
            <div class="row col-12 m-0 justify justify-content-between">
                <?php if ( have_rows( 'cadastro_de_formularios_de_contato' ) ) : ?>
                    <div class="nav flex-column nav-pills col-lg-4 background_first pill-titles" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <?php $count = 0;  while ( have_rows( 'cadastro_de_formularios_de_contato' ) ) : the_row(); ?>
                            <a class="nav-link <?php if ($count == 0): ?> active <?php endif;?>" id="v-pills-<?php echo $count;?>-tab" data-toggle="pill" href="#v-pills-<?php echo $count;?>" role="tab" aria-controls="v-pills-<?php echo $count;?>" aria-selected="true"> <?php the_sub_field( 'motivo_do_contato' ); ?> </a>
                                <hr class="hr_pill">
                        <?php $count++; endwhile; ?>
                    </div><!-- end nav pill -->
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>

                <?php if ( have_rows( 'cadastro_de_formularios_de_contato' ) ) : ?>
                    <div class="tab-content col-lg-7 h-100" id="v-pills-tabContent">
                        <?php $count = 0; while ( have_rows( 'cadastro_de_formularios_de_contato' ) ) : the_row(); ?>
                            <div class="tab-pane fade <?php if ($count == 0): ?> show active <?php endif;?> h-100" id="v-pills-<?php echo $count;?>" role="tabpanel" aria-labelledby="v-pills-<?php echo $count;?>-tab">
                                <?php the_sub_field('formulario_de_contato');?>
                            </div><!-- end active tab panel --> 
                        <?php $count++; endwhile; ?>     
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section><!-- /.institutional -->

<section class="motivo d-flex d-lg-none">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-lg-12 text_description">
                <h4 class="text-left"><strong><?php the_field( 'titulo_motivo_do_contato', $page_ID ); ?></strong></h4>
            </div><!-- /.col-lg-12 text_description -->
            <div id="accordion" class="accordion col-10">
                <?php if ( have_rows( 'cadastro_de_formularios_de_contato' ) ) : ?>
                    <?php $count = 0;  while ( have_rows( 'cadastro_de_formularios_de_contato' ) ) : the_row(); ?>
                        <div class="card mb-0">
                            <div class="card-header collapsed background_first padding_accordion_about mt-3" data-toggle="collapse" href="#collapse<?php echo $count;?>">
                                <a class="card-title collapse_title ">
                                    <?php the_sub_field( 'motivo_do_contato' ); ?> 
                                </a>
                            </div>
                            <div id="collapse<?php echo $count;?>" class="card-body collapse accordion_border mb-3" data-parent="#accordion">
                                <?php the_sub_field('formulario_de_contato');?>

                            </div>
                        </div>
                    <?php $count++; endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
            
        </div>
    </div>
</section><!-- /.institutional -->


<div class="d-none">
<?php echo do_shortcode('[searchandfilter id="1376"]');?>
</div>

<script>
//depending on where you add your JS, sometimes its necessary to wrap the above events in a function (as is standard practice):
(function ( $ ) {


    $(document).ready(function() { 
        $('.cidade-ligamos select, .imovel-ligamos select').find('option').remove().end().append('<option value="Selecione">Selecione</option>');
        $(".estado-ligamos select").on("click", function() {
            
            console.log('reset');
            $('#search-filter-form-1376 .sf-field-taxonomy-estado select option.sf-item-0').attr('selected', 'selected').trigger('change');
            $('#search-filter-form-1376 .sf-field-taxonomy-cidade select option.sf-item-0').attr('selected', 'selected').trigger('change');
            $('.imovel-ligamos select option.sf-item-0').attr('selected', 'selected').trigger('change');
            $('.cidade-ligamos select').find('option').remove().end().append('<option value="Selecione">Selecione a cidade</option>');
            $('.imovel-ligamos select').find('option').remove().end().append('<option value="Selecione">Selecione o imóvel</option>');
            setTimeout(function() { 
                var $options = $("#search-filter-form-1376 .sf-field-taxonomy-estado select option").clone();
                $('.estado-ligamos select').find('option').remove().end().append('');
                $('.estado-ligamos select').append($options);
                $('.imovel-ligamos select, #search-filter-form-1376 .sf-field-taxonomy-estado select, #search-filter-form-1376 .sf-field-taxonomy-cidade select').change(function(){
                let data= $(this).val();
                    console.log(data);            
                });
            }, 2000);
        });
        var $options = $("#search-filter-form-1376 .sf-field-taxonomy-estado select option").clone();
        $('.cidade-ligamos select').attr('disabled', 'disabled');
        $('.imovel-ligamos select').attr('disabled', 'disabled');
        $('.estado-ligamos select').append($options);
        $('.estado-ligamos select option').filter(':selected').val();
        $('.estado-ligamos select').on('change', function() {
            $('.cidade-ligamos select').attr('disabled', 'disabled');
            $('.imovel-ligamos select').attr('disabled', 'disabled');
            let select_estado = $('.estado-ligamos select option').filter(':selected').val();
            console.log(select_estado);
            $('#search-filter-form-1376 .sf-field-taxonomy-estado select option[value="'+select_estado+'"]').attr('selected', 'selected').trigger('change');
            $('#search-filter-form-1376 .sf-field-taxonomy-estado select').change(function(){
            let data= $(this).val();
                console.log(data);            
            });

            setTimeout(function() { 
                $('.cidade-ligamos select').find('option').remove().end().append('');
                let $options2 = $("#search-filter-form-1376 .sf-field-taxonomy-cidade select option").clone();
                $('.cidade-ligamos select').append($options2);
                $('.cidade-ligamos select option').filter(':selected').val();
                $('.cidade-ligamos select').removeAttr("disabled");
                $('.cidade-ligamos select').on('change', function() {
                    let select_cidade = $('.cidade-ligamos select option').filter(':selected').val();
                    console.log(select_cidade);
                    $('#search-filter-form-1376 .sf-field-taxonomy-cidade select option[value="'+select_cidade+'"]').attr('selected', 'selected').trigger('change');
                    $('#search-filter-form-1376 .sf-field-taxonomy-cidade select').change(function(){
                    let data= $(this).val();
                        console.log(data);            
                    });
                    setTimeout(function() { 
                        console.log('time');
                        $('.imovel-ligamos select').find('option').remove().end().append('');
                        console.log('remove');
                        let $options3 = $("#search-filter-form-1376 .sf-field-post-meta-nome_empreendimento select option").clone();
                           $('.imovel-ligamos select option.sf-item-0').attr('disabled', 'disabled');
                        $('.imovel-ligamos select').append($options3);
                        console.log('append');
                        $('.imovel-ligamos select').removeAttr("disabled");
                    }, 2000);
                });
            }, 2000);
        });   
        
    });
    $(document).ready(function() { 
        $('.cidade select, .imovel select').find('option').remove().end().append('<option value="Selecione">Selecione</option>');
        $(".estado select").on("click", function() {
            
            console.log('reset');
            $('#search-filter-form-1376 .sf-field-taxonomy-estado select option.sf-item-0').attr('selected', 'selected').trigger('change');
            $('#search-filter-form-1376 .sf-field-taxonomy-cidade select option.sf-item-0').attr('selected', 'selected').trigger('change');
            $('.imovel select option.sf-item-0').attr('selected', 'selected').trigger('change');
            $('.cidade select').find('option').remove().end().append('<option value="Selecione">Selecione a cidade</option>');
            $('.imovel select').find('option').remove().end().append('<option value="Selecione">Selecione o imóvel</option>');
            setTimeout(function() { 
                var $options = $("#search-filter-form-1376 .sf-field-taxonomy-estado select option").clone();
                $('.estado select').find('option').remove().end().append('');
                $('.estado select').append($options);
                $('.imovel select, #search-filter-form-1376 .sf-field-taxonomy-estado select, #search-filter-form-1376 .sf-field-taxonomy-cidade select').change(function(){
                let data= $(this).val();
                    console.log(data);            
                });
            }, 2000);
        });
        var $options = $("#search-filter-form-1376 .sf-field-taxonomy-estado select option").clone();
        $('.cidade select').attr('disabled', 'disabled');
        $('.imovel select').attr('disabled', 'disabled');
        $('.estado select').append($options);
        $('.estado select option').filter(':selected').val();
        $('.estado select').on('change', function() {
            $('.cidade select').attr('disabled', 'disabled');
            $('.imovel select').attr('disabled', 'disabled');
            let select_estado = $('.estado select option').filter(':selected').val();
            console.log(select_estado);
            $('#search-filter-form-1376 .sf-field-taxonomy-estado select option[value="'+select_estado+'"]').attr('selected', 'selected').trigger('change');
            $('#search-filter-form-1376 .sf-field-taxonomy-estado select').change(function(){
            let data= $(this).val();
                console.log(data);            
            });

            setTimeout(function() { 
                $('.cidade select').find('option').remove().end().append('');
                let $options2 = $("#search-filter-form-1376 .sf-field-taxonomy-cidade select option").clone();
                $('.cidade select').append($options2);
                $('.cidade select option').filter(':selected').val();
                $('.cidade select').removeAttr("disabled");
                $('.cidade select').on('change', function() {
                    let select_cidade = $('.cidade select option').filter(':selected').val();
                    console.log(select_cidade);
                    $('#search-filter-form-1376 .sf-field-taxonomy-cidade select option[value="'+select_cidade+'"]').attr('selected', 'selected').trigger('change');
                    $('#search-filter-form-1376 .sf-field-taxonomy-cidade select').change(function(){
                    let data= $(this).val();
                        console.log(data);            
                    });
                    setTimeout(function() { 
                        console.log('time');
                        $('.imovel select').find('option').remove().end().append('');
                        console.log('remove');
                        let $options3 = $("#search-filter-form-1376 .sf-field-post-meta-nome_empreendimento select option").clone();
                           $('.imovel select option.sf-item-0').attr('disabled', 'disabled');
                        $('.imovel select').append($options3);
                        console.log('append');
                        $('.imovel select').removeAttr("disabled");
                    }, 2000);
                });
            }, 2000);
        });   
    });

}(jQuery));
</script>