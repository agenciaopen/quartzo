<section class="blog d-none d-lg-block">
    <div class="container-fluid background_seventh h-100 p-5">
        <div class="d-flex flex-column h-100 align-items-center justify-content-center">
            <div class="col-5" data-aos="fade-up" data-aos-duration="10000" data-aos-once="true">
                <h2 class="text-center">Dicas para acertar na compra do apê</h2>
            </div>
            <hr class="hr_bot">
            <div class="col-4" data-aos="fade-up" data-aos-duration="10000" data-aos-once="true">
                <p class="text-center pb-4">
                    Aqui explicamos tudo o que você precisa saber sobre ter um novo apê!
                </p>
            </div><!-- end title/text -->
            <div class="row  align-items-center justify-content-center" data-aos="fade-up" data-aos-duration="10000" data-aos-once="true">
                <div class="col-4 text-center row justify-content-center align-items-center card_hover m-0">
                    <div class="col-12 blog_img" style="background-image: url('/wp-content/themes/quartzo/img/NoPath.png')"></div>
                    <div class="col-10 card p-0">
                        <div class="card-header">
                            <p class="blog_card_date">04 Junho</p>
                            <h4 class="blog_card_title">FGTS: 9 respostas sobre como usar para comprar a sua casa própria</h4>
                        </div>
                        <p class="blog_card_text px-3 py-4">O saldo disponível em seu FGTS (Fundo de Garantia por Tempo de Serviço) pode ajudar você a sair do aluguel, já que essa poupança...</p>
                        <div class="col-12 text-center text-lg-right px-3 pb-4">
                            <a class="btn_second_text" href="#" role="button">
                                <div class="btn btn_blog col-10 col-lg-6 ">Ler mais</div>
                            </a>
                        </div>
                    </div>
                </div><!-- end card -->
                <div class="col-4 text-center row justify-content-center align-items-center card_hover m-0">
                    <div class="col-12 blog_img" style="background-image: url('/wp-content/themes/quartzo/img/NoPath - Copia (4).png')"></div>
                    <div class="col-10 card p-0">
                        <div class="card-header">
                            <p class="blog_card_date">04 Junho</p>
                            <h4 class="blog_card_title">Como saber que é uma boa hora para comprar um imóvel?</h4>
                        </div>
                        <p class="blog_card_text px-3 py-4">Procurando pelo apartamento perfeito? Andou pesquisando, mas ainda tem dúvidas de como saber se você está acertando na compra...</p>
                        <div class="col-12 text-center text-lg-right px-3 pb-4">
                            <a class="btn_second_text" href="#" role="button">
                                <div class="btn btn_blog col-10 col-lg-6 ">Ler mais</div>
                            </a>
                        </div>
                    </div>
                </div><!-- end card -->
                <div class="col-4 text-center row justify-content-center align-items-center card_hover m-0">
                    <div class="col-12 blog_img" style="background-image: url('/wp-content/themes/quartzo/img/NoPath - Copia (5).png')"></div>
                    <div class="col-10 card p-0">
                        <div class="card-header">
                            <p class="blog_card_date">04 Junho</p>
                            <h4 class="blog_card_title">Bebê a caminho: 5 dicas para comprar um novo imóvel</h4>
                        </div>
                        <p class="blog_card_text px-3 py-4">Se você está planejando ter filhos ou já está à espera do seu primeiro, observe alguns pontos antes de optar pelo imóvel ideal. Confira nossas...</p>
                        <div class="col-12 text-center text-lg-right px-3 pb-4">
                            <a class="btn_second_text" href="#" role="button">
                                <div class="btn btn_blog col-10 col-lg-6 ">Ler mais</div>
                            </a>
                        </div>
                    </div>
                </div><!-- end card -->
                <div class="col-12 text-center pt-5">
                    <a class="btn_second_text" href="https://vivaquartzo.com.br/blog/" role="button" target="_blank">
                        <div class="btn btn_second col-12 col-lg-3">Ver todos os posts</div>
                    </a>
                </div>
            </div><!-- /.col-12 -->
        </div>
    </div><!-- /container -->
</section><!-- /.blog -->