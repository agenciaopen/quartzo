<section class="empreendimentos ">

    <div class="container h-100">

        <div class=" h-100 align-items-center justify-content-start">

            <div class="row justify-content-start align-items-strech m-0 p-3">

                <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->

                <div class=" col-2 col-lg-1 hr-left"></div>

                <h2 class="col-11 col-lg-5">nossos lançamentos</h2>

                <p class="col-12 px-0 py-3">Confira os nossos últimos lançamentos:</p>

            </div><!-- /.col-12 row justify-content-start aling-items-strech -->

            <div class="col-lg-12 carousel_home">

            <?php

                    // Custom WP query query

                    // Query Arguments

                    $args_query = array(

                        'post_status' => array('publish'),

                        'posts_per_page' => 5,

                        'post_type' => 'empreendimentos',

                        'order' => 'DESC',

                        'orderby' => 'title',

                        

    'meta_query' => array(

        array(

            // 'key' => 'destacar',

            // 'value'   => '1',

            // 'compare' => 'LIKE'

        )

    )

                    );



                    // The Query

                    $query = new WP_Query($args_query);



                    // The Loop

                    if ($query->have_posts()) {

                        $count = 0;

                        $animate = 100;

                        while ($query->have_posts()) {

                            $query->the_post();

                            // Your custom code 

                    ?>

                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">

                    <div class="text-center item"  data-aos="fade-up"

                    data-aos-delay="<?php echo $animate;?>"

                    data-aos-duration="500"

                    data-aos-mirror="true"

                    data-aos-once="true"

                    data-aos-anchor-placement="top-center">

                        <div class="card <?php if ($count == 0) : ?>purple background_seventh <?php endif;?> <?php if ($count == 1) : ?> blue background_seventh <?php endif;?><?php if ($count == 2): ?> orange background_seventh <?php endif;?><?php if($count == 3): ?> green background_seventh <?php endif;?>">

                            <div class="card-header background_first">

                                <div class="col-12 text-center">

                                    <h4 class="card_titulo"><?php the_title(''); ?></h4>

                                </div>

                            </div><!-- end card header -->

                            <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'medium_large');
                                $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'medium_large'); 

                                        if ( $featured_img_url ) :

                                            $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'medium_large');
                                            $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'medium_large'); 

                                        else :

                                            $url = '/wp-content/themes/quartzo/img/QUARTZO - ED. Alto Boa Vista - Torres Noturna.png';
                                            $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'medium_large'); 

                                        endif;

                                    ?>

                            <img class="card-img-top img-fluid img_filter" src="<?php echo $featured_img_url;?>" alt="<?php the_title(''); ?>" title="<?php the_title(''); ?>">

                            <div class="card-body row m-0  justify-content-center align-items-center">

                                <div class="card_fa col-12 text-center p-0 card_img_border">

                                    <i class="fa fa-map-marker"></i>

                                    <?php the_field( 'localizacao' ); ?>

                                    <hr class="hr_card">

                                </div>

                                <div class="row col-12 align-items-center justify-content-center">

                                <?php

                        $terms = get_the_terms($post->ID, 'status');

                        $i = 0;



                            if( $terms):

                                foreach( $terms as $t ):

                        ?>

                                    <div class="col-12 col-lg-6">

                                        <div class="card_badge col-12">

                                       

                        

									<?php

									$taxonomy_prefix = 'status';

									$term_id = $t->term_id ;

									$term_id_prefixed = $taxonomy_prefix .'_'. $term_id;



									?>

									                    <?php echo $t->name;?>





                                        </div>

                                    </div>

                                    <?php

                                    $i++;



                                endforeach;

                            endif;

                        ?>

                                <?php if ($i > 1) : ?>

                                <?php else : ?>

                                        <?php if (get_field('quartos')) : ?> 

                                        <div class="col-12 col-lg-5">

                                            <div class="card_badge col-12">

                                            <?php the_field( 'quartos' ); ?>



                                            </div>

                                        </div>

                                    <?php endif; endif ;?>

                                </div>

                                <div class="row col-12 align-items-center justify-content-center my-4 animate__animated animate__bounce">

                                    <?php if ( have_rows( 'cadastro_de_itens_ficha_tecnica_' ) ) : ?>

                                        <?php while ( have_rows( 'cadastro_de_itens_ficha_tecnica_' ) ) : the_row(); ?>

                                            <?php if ( get_sub_field( 'destacar' ) == 1 ) : ?>

                                                <?php if ( get_sub_field( 'icone' ) ) : ?>

                                                    <div class="col-3">

                                                        <img src="<?php the_sub_field( 'icone' ); ?>" class="img-fluid" alt="<?php the_sub_field( 'item' ); ?>" title="<?php the_sub_field( 'item' ); ?>" />

                                                    </div>

                                                <?php endif ?>

                                            <?php else : ?>

                                                <?php // echo 'false'; ?>

                                            <?php endif; ?>

                                        <?php endwhile; ?>

                                    <?php else : ?>

                                        <?php // no rows found ?>

                                    <?php endif; ?>

                                </div>

                            </div>

                        </div><!-- end card -->

                    </div>

                </a>

                           



                    <?php                        $animate += 100;  $count++;  }

                    } else {

                        // no posts found



                    }



                    /* Restore original Post Data */

                    wp_reset_postdata();



                    ?>

               

               

             

            </div><!-- end row -->

        </div>

    </div>

    <div class="d-none d-lg-flex col-12 justify-content-center mt-5">

        <div class="text-center col-lg-4">

            <a class="btn_second_text" href="/empreendimentos" role="button">

                <div class="btn btn_second btn_wide ">mostrar mais imoveis </div>

            </a>

        </div>

    </div>

</section><!-- /.empreendimentos -->