<section class="beneficios d-none d-lg-block" data-aos="fade-up" data-aos-duration="10000">
    <div class="container h-100 ">
        <div class="row h-100 align-items-center justify-content-center beneficios_img">
            <div class="col-lg-6 text-white mt-5">
                <div class="col-12 row justify-content-start align-items-strech">
                    <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->
                    <div class=" col-lg-1 hr_left_white text-white"></div>
                    <h2 class="col-lg-11 text-white title_small pr-0 ">
                    <?php the_field('titulo_beneficios_mcmv');?>

                    </h2>
                </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                <p class="text-white mt-3 text_small">
                    <?php the_field('descricao_beneficios_mcmv', false, false);?>
                </p>
            </div><!-- /.col-lg-6 text_description -->
            <div class="col-lg-2 p-0 d-flex justify-content-start align-items-center">
                <img src='/wp-content/uploads/2020/12/Design-sem-nome-17.png' class='img-fluid mcva' alt='beneficios mcmv' title='beneficios mcmv' lazy='loading'>
            </div>
            <div class="col-12 row justify-content-center aling-items-center mt-4 mb-5">
                <div class="text-center col-4 p-0">
                    <a class="btn_second_text" href="/financiamentos" role="button">
                        <div class="btn btn_second btn_beneficios">Conhecer os benefícios</div>
                    </a>
                </div>
            </div>
        </div><!-- /.col-lg-6 text_description -->
    </div>
</section><!-- /.fale -->