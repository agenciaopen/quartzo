<section class="simulacao background_nineth mb-3 mb-lg-0 d-none d-lg-block">
    <div class="container h-100">
        <div class="row h-100 align-items-start justify-content-around mx-1">
            <div class="col-12 col-lg-3 d-flex flex-column justify-content-center align-items-center" data-aos="fade-up" data-aos-duration="10000" data-aos-once="true">
                <img src='/wp-content/themes/quartzo/img/Grupo 1459.png' class='img-fluid p-4 mt-2' alt='simulacao' title='beneficios mcmv' lazy='loading'>
                <div class="row align-items-stretch justify-content-start m-0">
                    <div class=" col-lg-1 hr_left_white text-white"></div>
                    <h2 class="text-white col-11 title_small">Faça uma simulação</h2>
                </div>
                <p class="text-white my-3 text_small">
                    Preencha o formulário e encontre um apê que cabe no seu bolso!
                </p>
            </div>
            <div class="col-12 col-lg-8 align-items-between" data-aos="fade-up" data-aos-duration="10000" data-aos-once="true">
                <?php echo do_shortcode('[contact-form-7 id="1273" title="Faça uma simulação"]');?>
            </div>
            <form class="col-12 col-lg-8 align-items-between d-none" data-aos="fade-up" data-aos-duration="10000" data-aos-once="true">
                <div class="form-row">
                    <div class="col-12 col-lg-6 my-3">
                        <input type="text" class="form-control p-3" placeholder="Nome">
                    </div>
                    <div class="col-12 col-lg-6  my-3">
                        <input type="email" class="form-control p-3" placeholder="E-mail">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 col-lg-6  my-3">
                        <input type="tel" class="form-control p-3" placeholder="Telefone">
                    </div>
                    <div class="col-12 col-lg-6  my-3">
                        <input type="text" class="form-control p-3" placeholder="Renda mensal">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 col-lg-6  my-3">
                        <input type="text" class="form-control p-3" placeholder="Preço do Imóvel">
                    </div>
                    <div class="col-12 col-lg-6  my-3">
                        <input type="text" class="form-control p-3" placeholder="Cidade">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 col-lg-6  my-3">
                        <input type="text" class="form-control p-3" placeholder="Estado Civil">
                    </div>
                    <div class="col-12 col-lg-6  my-3">
                        <input type="text" class="form-control p-3" placeholder="Saldo FGTS">
                    </div>
                </div>
                <div class="col-12 text-center p-0  my-5">
                    <a class="btn_second_text" href="#" role="button">
                        <div class="btn btn_second col-12 col-lg-6 ">Simular</div>
                    </a>
                </div>
            </form>
        </div><!-- /.col-12 -->
    </div><!-- /container -->
</section><!-- /.simulacao -->