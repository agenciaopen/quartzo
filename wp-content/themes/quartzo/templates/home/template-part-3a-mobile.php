<section class="simulacaoMobile background_nineth mb-3 d-block d-lg-none py-3">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center mx-1 beneficios_borda">
            <a class="btn col-12" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                <img src='/wp-content/themes/quartzo/img/Grupo 1459.png' class='img-fluid mt-2' alt='simulacao' title='beneficios mcmv' lazy='loading'>
                <p class="simulacao_mobile_header_text text-white text-center col-12 ">Simulação</p>
            </a>
            <div class="collapse" id="collapseExample">
                <div class="col-12 col-lg-3 d-flex flex-column justify-content-center align-items-center">
                    <div class="row align-items-stretch justify-content-start m-0">
                        <div class=" col-lg-1 hr_left_white text-white"></div>
                    </div>
                    <p class="text-white my-3 text_small">
                        Preencha o formulário e encontre um apê que cabe no seu bolso!
                    </p>
                </div>
                <div class="col-12 col-lg-8 align-items-between">
                    <?php echo do_shortcode('[contact-form-7 id="1273" title="Faça uma simulação"]'); ?>
                </div>
            </div>
        </div><!-- /collapse -->
    </div><!-- /container -->
</section><!-- /.simulacao -->