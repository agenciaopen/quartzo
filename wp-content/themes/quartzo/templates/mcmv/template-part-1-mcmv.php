<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="mcmv">
    <div class="container h-100 ">
        <div class="row h-100 align-items-start justify-content-between m-0">
            <?php if (get_field('desativar_mcmv', $page_ID)): ?>
            <?php else : ?>
                <div class="d-none d-lg-inline col-lg-6 text_description">
                    <div class="col-12 row justify-content-start aling-items-strech p-0 pb-3 m-0">
                        <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->
                        <div class=" col-lg-1 hr-left"></div>
                        <h2 class="col-lg-11"><?php the_field( 'titulo_mcmv', $page_ID ); ?></h2>
                    </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                    <p class="pb-3">
                        <?php the_field( 'descricao_mcmv', $page_ID  ); ?>
                    </p>
                    <div class="d-flex justify-content-end text-right pt-3 pt-lg-5 mt-lg-5">
                        <div class="btn_mcmv">
                            <a class="btn_second_text" href="#" role="button">
                                <div class="btn btn_second">Falar com um consultor</div>
                            </a>
                        </div>
                    </div>
                </div><!-- /.col-lg-6 text_description -->
                <div class="col-12 col-lg-5 text-center p-0">
                    <?php if ( get_field( 'selo_mcmv' ) ) : ?>
                        <img src="<?php the_field( 'selo_mcmv' ); ?>" class='img-fluid mcmv_selo' alt='selo' title='selo' lazy='loading'>
                    <?php else : ?>
                        <img src='/wp-content/themes/quartzo/img/Grupo 1693.png' class='img-fluid mcmv_selo' alt='selo' title='selo' lazy='loading'>
                    <?php endif; ?>
                    <?php $imagem_mcmv = get_field( 'imagem_mcmv' ); ?>
                    <?php if ( $imagem_mcmv ) : ?>
                        <img src="<?php echo esc_url( $imagem_mcmv['url'] ); ?>" alt="<?php echo esc_attr( $imagem_mcmv['alt'] ); ?>" class='img-fluid' alt='mcmv' title='mcmv' lazy='loading'>
                    <?php else : ?>
                        <img src='/wp-content/themes/quartzo/img/shutterstock_1446065777.png' class='img-fluid' alt='mcmv' title='mcmv' lazy='loading'>
                    <?php endif; ?>

                </div>
            <?php endif;?>

            <div class="d-block d-lg-inline col-lg-6 text_description mt-5 order-2">
                <div class="col-12 row justify-content-start aling-items-strech p-0 pb-3 m-0">
                    <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->
                    <div class=" col-2 col-lg-1 hr-left"></div>
                    <h2 class="col-10 col-lg-11"><?php the_field( 'titulo_mcmv_copiar', $page_ID ); ?></h2>
                </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                <a class="collapse_mcmv_mobile collapsed" data-toggle="collapse" data-target="#viewdetails3" aria-expanded="false" >
                <p class="pb-3">
                    <?php the_field( 'descricao_mcmv_copiar', $page_ID  ); ?>
                    <span class="collapse" id="viewdetails3"> <?php the_field( 'descricao_mcmv_copiar', $page_ID  ); ?> </span> 
                </p>
                </a>
                <div class="d-flex justify-content-end text-right pt-3 pt-lg-5 mt-lg-5">
                    <div class="btn_mcmv">
                        <a class="btn_second_text" href="#" role="button">
                            <div class="btn btn_second">Falar com um consultor</div>
                        </a>
                    </div>
                </div>
            </div><!-- /.col-lg-6 text_description -->
            <div class="col-12 col-lg-5 text-center p-0 mt-5 order-1 d-none d-lg-inline">
                <?php if ( get_field( 'selo_mcmv_copiar' ) ) : ?>
                    <img src="<?php the_field( 'selo_mcmv_copiar' ); ?>" class='img-fluid mcmv_selo_copiar' alt='selo' title='selo' lazy='loading'>
                <?php else : ?>
                    <img src='/wp-content/themes/quartzo/img/Grupo 1693.png' class='img-fluid mcmv_selo_copiar' alt='selo' title='selo' lazy='loading'>
                <?php endif; ?>
                <?php $imagem_mcmv = get_field( 'imagem_mcmv_copiar' ); ?>
                <?php if ( $imagem_mcmv ) : ?>
                    <img src="<?php echo esc_url( $imagem_mcmv['url'] ); ?>" alt="<?php echo esc_attr( $imagem_mcmv['alt'] ); ?>" class='img-fluid' alt='mcmv' title='mcmv' lazy='loading'>
                <?php else : ?>
                    <img src='/wp-content/themes/quartzo/img/shutterstock_1446065777.png' class='img-fluid' alt='mcmv' title='mcmv' lazy='loading'>
                <?php endif; ?>

            </div>

        </div>
    </div>
</section><!-- /.mcmv -->