<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="duvidasColapse pt-0">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <?php if ( have_rows( 'cadastro_de_perguntas_e_respostas', $page_ID ) ) : ?>
                <div id="accordion" class="accordion duvidas col-10 p-0">
                    <?php $count = 1; while ( have_rows( 'cadastro_de_perguntas_e_respostas', $page_ID ) ) : the_row(); ?>
                        <div class="card mb-0">
                            <div class="card-header d-flex align-items-start justify-content-start collapsed background_first padding_accordion_about mt-3 " data-toggle="collapse" href="#collapseOne<?php echo $count;?>">
                                <div class="d-inline-block">
                                    <span class="fa-stack fa-sm text-align-left">
                                        <i class="fa fa-circle fa-stack-2x"></i>
                                        <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <a class="card-title collapse_title d-inline-block">
                                    <?php echo $count;?>. <?php the_sub_field( 'pergunta' ); ?>
                                </a>
                            </div><!-- end callapse header -->
                            <div id="collapseOne<?php echo $count;?>" class="card-body collapse accordion_border p-4 mb-3" data-parent="#accordion">
                                <p class="collapse_text col-12">
                                    <?php the_sub_field( 'resposta' ); ?>
                                </p>
                            </div><!-- end collapse -->
                        </div>
                    <?php $count++; endwhile; ?>
                </div>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
        </div> <!-- end accordion -->
        <hr class="d-none d-lg-block hr_gray">
    </div><!-- end row -->
</section><!-- /.institutional -->