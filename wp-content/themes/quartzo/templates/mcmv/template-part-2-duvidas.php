<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="duvidas">
    <div class="container h-100">
        <div class=" row h-100 text-center align-items-center justify-content-center">
            <div class="col-lg-5">
                <h2 class="text-center"><?php the_field( 'titulo_duvidas_frequentes', $page_ID ); ?></h2>
                <hr class="hr_bot">
            </div>
            <div class="w-100"></div>
            <div class="col-lg-5">
                <p class="text-center">
                    <?php the_field( 'descricao_duvidas_frequentes', $page_ID ); ?>

                </p>
            </div>
        </div>
    </div>
</section><!-- /.duvidas -->

