<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="corretor pb-0">
    <div class="container h-100 background-first px-0">
        <div class="row m-0 h-100 align-items-center justify-content-start">
            <div class="col-12 col-lg-6 align-items-strech row m-0">
                <div class="col-2 col-lg-1 hr-left"></div>
                <div class="col-10 col-lg-11 text-center">
                    <h2><?php the_field( 'titulo_fale_com_o_corretor', $page_ID ); ?></h2>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.corretor -->