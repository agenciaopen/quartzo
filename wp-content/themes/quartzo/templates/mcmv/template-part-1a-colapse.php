<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="mcmv_colapse d-block d-lg-none">
    <div class="container h-100 ">
        <div class="row h-100 align-items-center justify-content-center">
            <div id="accordion" class="accordion col-10">
            <?php if (get_field('desativar_mcmv', $page_ID)): ?>
            <?php else : ?>
                <div class="card mb-0">
                    <div class="card-header collapsed background_first padding_accordion_about mt-3" data-toggle="collapse" href="#collapseFour">
                        <a class="card-title collapse_title">
                            <?php the_field( 'titulo_mcmv', $page_ID ); ?>
                        </a>
                    </div>
                    <div id="collapseFour" class="card-body collapse accordion_border mb-3" data-parent="#accordion">
                        <p class="collapse_text col-12 py-4">
                            <?php the_field( 'descricao_mcmv', $page_ID  ); ?>
                        </p>
                        <div class="btn_mcmv text-center">
                            <a class="btn_second_text mx-auto" href="#" role="button">
                                <div class="btn btn_second">quero saber mais </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-5 text-center p-0 mt-5 order-1  d-lg-none">
                    <?php if ( get_field( 'selo_mcmv_copiar' ) ) : ?>
                        <img src="<?php the_field( 'selo_mcmv_copiar' ); ?>" class='img-fluid mcmv_selo_copiar' alt='selo' title='selo' lazy='loading'>
                    <?php else : ?>
                        <img src='/wp-content/themes/quartzo/img/Grupo 1693.png' class='img-fluid mcmv_selo_copiar' alt='selo' title='selo' lazy='loading'>
                    <?php endif; ?>
                    <?php $imagem_mcmv = get_field( 'imagem_mcmv_copiar' ); ?>
                    <?php if ( $imagem_mcmv ) : ?>
                        <img src="<?php echo esc_url( $imagem_mcmv['url'] ); ?>" alt="<?php echo esc_attr( $imagem_mcmv['alt'] ); ?>" class='img-fluid' alt='mcmv' title='mcmv' lazy='loading'>
                    <?php else : ?>
                        <img src='/wp-content/themes/quartzo/img/shutterstock_1446065777.png' class='img-fluid' alt='mcmv' title='mcmv' lazy='loading'>
                    <?php endif; ?>

                </div>
            <?php endif;?>
                <div class="card mb-0">
                    <div class="card-header collapsed background_first padding_accordion_about mt-3" data-toggle="collapse" href="#collapseFour2">
                        <a class="card-title collapse_title">
                            <?php the_field( 'titulo_mcmv_copiar', $page_ID ); ?>
                        </a>
                    </div>
                    <div id="collapseFour2" class="card-body collapse accordion_border mb-3" data-parent="#accordion">
                        <p class="collapse_text col-12 py-4">
                            <?php the_field( 'descricao_mcmv_copiar', $page_ID  ); ?>
                        </p>
                        <div class="btn_mcmv text-center">
                            <a class="btn_second_text mx-auto" href="#" role="button">
                                <div class="btn btn_second">quero saber mais </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.mcmv -->