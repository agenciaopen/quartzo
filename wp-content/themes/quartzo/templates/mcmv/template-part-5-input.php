<?php 
global $post;
$page_ID = $post->ID;


?>
<section class="input">
    <div class="container h-100 ">
        <div class="row h-100 align-items-center justify-content-center m-0">
            <div class="col-12 col-lg-6 text-center">
                <?php if ( get_field( 'imagem_fale_com_o_corretor', $page_ID ) ) : ?>
                    <img src='<?php the_field( 'imagem_fale_com_o_corretor', $page_ID ); ?>' class='img-fluid' alt='<?php the_field( 'titulo_fale_com_o_corretor', $page_ID ); ?>' title='<?php the_field( 'titulo_fale_com_o_corretor', $page_ID ); ?>' lazy='loading'>

                <?php else : ?>
                    <img src='/wp-content/themes/quartzo/img/shutterstock_1432087025.png' class='img-fluid' alt='<?php the_field( 'titulo_fale_com_o_corretor', $page_ID ); ?>' title='<?php the_field( 'titulo_fale_com_o_corretor', $page_ID ); ?>' lazy='loading'>
                <?php endif; ?>
            </div>
            <div class="mt-4 mt-lg-0 col-12 col-lg-6 row justify-content-start aling-items-center m-0">
                <div class="input-group mb-4 col-12">
                    <input type="text" class="form-control" placeholder="Nome" aria-label="Nome" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-4 col-12 col-lg-6">
                    <input type="email" class="form-control" placeholder="E-mail" aria-label="E-mail" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-4 col-12 col-lg-6">
                    <input type="tel" class="form-control" placeholder="Telefone" aria-label="Telefone" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-4 col-12 col-lg-6">
                    <input type="email" class="form-control" placeholder="Cidade" aria-label="Cidade" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-4 col-12 col-lg-6">
                    <input type="tel" class="form-control" placeholder="Estado Civil" aria-label="Estado Civil" aria-describedby="basic-addon1">
                </div>
                <div class="btn_mcmv col-12 text-center p-lg-5">
                    <a class="btn_second_text" href="#" role="button">
                        <div class="btn btn_second">quero saber mais </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.input -->