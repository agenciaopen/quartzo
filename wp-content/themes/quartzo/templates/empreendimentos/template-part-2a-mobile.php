<?php 
global $post;
$page_ID = $post->ID;

$page_ID = '15';

?>
<section class="portfolioMobile d-block d-lg-none pt-0">
    <div class="container h-100 ">
        <div class="row h-100 align-items-center justify-content-center">
            <a class="col-12 py-3 text-decoration-none portfolio_collapse_header text-center" type="button" data-toggle="collapse" data-target="#collapsePortfolio" aria-expanded="false" aria-controls="collapsePortfolio">
                <div class="portfolio_border col-12 p-0">
                    <p class="text-white col-5 text-decoration-none p-3">Veja tudo que nós já fizemos</p>
                </div>
            </a>
            <div class="collapse" id="collapsePortfolio">
                <div class="d-lg-none col-lg-6 text_description pt-5">
                    <div class="col-12 row justify-content-start aling-items-strech ">
                        <div class=" col-2 hr-left"></div>
                        <h2 class="col-10"><?php the_field('titulo_portfolio', $page_ID); ?></h2>
                    </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                    <?php the_field('titulo_portfolio_copiar', $page_ID); ?>

                </div><!-- /.col-lg-6 text_description -->
                <div class="col-12 col-lg-5 text-center carousel_portfolio_mobile p-0 h-100">
                    <?php if (have_rows('portfolio', $page_ID)) : ?>
                        <?php while (have_rows('portfolio', $page_ID)) : the_row(); ?>

                            <a class="p-3" href="<?php the_sub_field('imagem'); ?>" data-fancybox="portfolio" data-caption="Caption #1">
                                <?php if (get_sub_field('imagem')) : ?>
                                    <img class="col-lg-12 p-0 img-fluid" src="<?php the_sub_field('imagem'); ?>" />
                                <?php else : ?>
                                    <img class="col-lg-12 p-0 img-fluid" src="/wp-content/themes/quartzo/img/4.png" alt="" />
                                <?php endif ?>
                            </a>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>


                </div><!-- end col-12 col-lg-5 -->
            </div>
        </div>
    </div>
</section><!-- /.mcmv -->