<?php 
global $post;
$page_ID = $post->ID;

$page_ID = '15';

?>
<section class="portfolio d-none d-lg-block">
    <div class="container h-100 ">
        <div class="row h-100 align-items-start justify-content-between">
            <div class="d-none d-lg-inline col-lg-6 text_description ">
                <div class="col-12 row justify-content-start aling-items-strech ">
                    <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->
                    <div class=" col-lg-1 hr-left"></div>
                    <h2 class="col-lg-11"><?php the_field( 'titulo_portfolio', $pageID ); ?></h2>
                </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                <?php the_field( 'titulo_portfolio_copiar', $pageID ); ?>

            </div><!-- /.col-lg-6 text_description -->
            <div class="col-12 col-lg-5 text-center carousel_portfolio p-0 h-100">
            <?php if ( have_rows( 'portfolio', $pageID ) ) : ?>
                <?php while ( have_rows( 'portfolio', $pageID ) ) : the_row(); ?>
                
                    <a class="p-1" href="<?php the_sub_field( 'imagem' ); ?>" data-fancybox="portfolio" data-caption="Caption #1">
                        <?php if ( get_sub_field( 'imagem' ) ) : ?>
                            <img class="col-lg-12 p-0 img-fluid" src="<?php the_sub_field( 'imagem' ); ?>" />
                        <?php else :?>
                            <img class="col-lg-12 p-0 img-fluid" src="/wp-content/themes/quartzo/img/4.png" alt="" />
                        <?php endif ?>    
                        <div class="hover_portfolio">
                            <div class="hover_portfolio_text d-flex flex-column justify-content-center align-items-center">
                                <img src='/wp-content/themes/quartzo/img/resize-1.png' class='img-fluid mb-4' alt='' title='' lazy='loading'>
                                <?php the_sub_field( 'titulo' ); ?>
                            </div>
                        </div>
                    </a>
                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
                

            </div><!-- end col-12 col-lg-5 -->
        </div>
    </div>
</section><!-- /.mcmv -->