<section class="empreendimentos">

    <div class="container h-100">

        <div class="h-100 align-items-center justify-content-center">

            <div class="row m-0 pb-5 col-12 justify-content-start aling-items-strech p-0">

                <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->

                <div class=" col-2 col-lg-1 hr-left"></div>

                <h1 class="col-10 col-lg-11">Encontre o seu empreendimento</h1>

            </div><!-- /.col-12 row justify-content-start aling-items-strech -->



            <div class="col-12 p-lg-0">

                <ul class="nav nav-pills mb-3 nav-fill background_seventh" id="pills-tab" role="tablist">

                    <?php

                    $terms = get_terms(

                        array(

                            'taxonomy' => 'status',

                            'hide_empty' => 'false',

                            'order' => 'ASC',

                            'orderby' => 'meta_value_num',

                            'meta_query' => array(

                                array(

                                    'key' => 'ordem'

                                )

                            )



                        )

                    );

                    if ($terms) :

                        $cont = 1;

                        foreach ($terms as $t) :

                    ?>

                            <li class="nav-item">

                                <a class="nav-item nav-link <?php echo $cont == 1 ? 'active' : ''; ?>" id="empreendimentos-<?php echo $t->slug; ?>-tab" data-toggle="tab" href="#empreendimentos-<?php echo $t->slug; ?>" role="tab" aria-controls="empreendimentos-<?php echo $t->slug; ?>" aria-selected="<?php echo $cont == 1 ? 'true' : 'false'; ?>">

                                    <?php echo $t->name; ?>

                                </a>

                                <hr class="hr_gray col-10 my-0 mx-auto d-block d-lg-none">

                            </li>

                    <?php

                            $cont++;

                        endforeach;

                    endif;

                    ?>

                </ul>



            </div>

            <div class="tab-content" id="nav-tabContent">

                <?php

                $terms = get_terms(

                    array(

                        'taxonomy' => 'status',

                        'hide_empty' => 'false',

                        'order' => 'ASC',

                        'orderby' => 'meta_value_num',

                        'meta_query' => array(

                            array(

                                'key' => 'ordem'

                            )

                        )





                    )

                );

                if ($terms) :

                    $cont = 1;

                    foreach ($terms as $t) :

                ?>

                        <div class="tab-pane fade <?php echo $cont == 1 ? 'show active' : ''; ?>" id="empreendimentos-<?php echo $t->slug; ?>" role="tabpanel" aria-labelledby="empreendimentos-<?php echo $t->slug; ?>-tab">

                            <div class="col-lg-12 <?php if (wp_is_mobile()) : ?> carousels_empreendimentos <?php endif; ?> p-0 row m-0">

                                <?php

                                // Custom WP query query

                                // Query Arguments

                                $args_query = array(

                                    'post_status' => array('publish'),

                                    'posts_per_page' => -1,

                                    'post_type' => 'empreendimentos',

                                    // 'meta_query'	=> array(

                                    //     array(

                                    //         'key'	  	=> 'hidden_emp',

                                    //         'value'	  	=> '0',

                                    //         'compare' 	=> 'LIKE',

                                    //     ),

                                    // ),

                                    'tax_query' => array(

                                        array(

                                            'taxonomy' => 'status',

                                            'field'    => 'slug',

                                            'terms'    => $t->slug,



                                        ),

                                    )

                                );



                                // The Query

                                $query_emp = new WP_Query($args_query);



                                // The Loop

                                if ($query_emp->have_posts()) {

                                    $count = 0;

                                    $animate = 100;

                                    while ($query_emp->have_posts()) {

                                        $query_emp->the_post();

                                        // Your custom code 

                                ?>

                                        <a class="col-md-6 col-lg-4 col-12 mb-lg-4" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">

                                            <div class="text-center item" data-aos="fade-up" data-aos-delay="<?php echo $animate; ?>" data-aos-duration="500" data-aos-mirror="true" data-aos-once="true" data-aos-anchor-placement="top-center">

                                                <div class="card <?php if ($count == 0) : ?>purple background_seventh <?php endif; ?> <?php if ($count == 1) : ?> blue background_seventh <?php endif; ?><?php if ($count == 2) : ?> orange background_seventh <?php endif; ?><?php if ($count == 3) : ?> green background_seventh <?php endif; ?>">

                                                    <div class="card-header background_first">

                                                        <div class="col-12 text-center">

                                                            <h4 class="card_titulo"><?php the_title(''); ?></h4>

                                                        </div>

                                                    </div><!-- end card header -->

                                                    <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');

                                                    if ($url) :

                                                        $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');

                                                    else :

                                                        $url = '/wp-content/themes/quartzo/img/QUARTZO - ED. Alto Boa Vista - Torres Noturna.png';

                                                    endif;

                                                    ?>

                                                    <img class="card-img-top img-fluid img_filter" src="<?php echo $url; ?>" alt="<?php the_title(''); ?>" title="<?php the_title(''); ?>">

                                                    <div class="card-body row m-0  justify-content-center align-items-center">

                                                        <div class="card_fa col-12 text-center p-0 card_img_border">

                                                            <i class="fa fa-map-marker"></i>

                                                            <?php the_field('localizacao'); ?>

                                                            <hr class="hr_card">

                                                        </div>

                                                        <div class="row col-12 align-items-center justify-content-center">

                                                            <?php

                                                            $terms = get_the_terms($post->ID, 'status');

                                                            $i = 0;



                                                            if ($terms) :

                                                                foreach ($terms as $t) :

                                                            ?>

                                                                    <div class="col-12 col-lg-6">

                                                                        <div class="card_badge col-12">





                                                                            <?php

                                                                            $taxonomy_prefix = 'status';

                                                                            $term_id = $t->term_id;

                                                                            $term_id_prefixed = $taxonomy_prefix . '_' . $term_id;



                                                                            ?>

                                                                            <?php echo $t->name; ?>





                                                                        </div>

                                                                    </div>

                                                            <?php

                                                                    $i++;



                                                                endforeach;

                                                            endif;

                                                            ?>

                                                            <?php if ($i > 1) : ?>

                                                            <?php else : ?>

                                                                <?php if (get_field('quartos')) : ?>

                                                                    <div class="col-12 col-lg-5">

                                                                        <div class="card_badge col-12">

                                                                            <?php the_field('quartos'); ?>



                                                                        </div>

                                                                    </div>

                                                            <?php endif;

                                                            endif; ?>

                                                        </div>

                                                        <div class="row col-12 align-items-center justify-content-center my-4 animate__animated animate__bounce">

                                                            <?php if (have_rows('cadastro_de_itens_ficha_tecnica_')) : ?>

                                                                <?php while (have_rows('cadastro_de_itens_ficha_tecnica_')) : the_row(); ?>

                                                                    <?php if (get_sub_field('destacar') == 1) : ?>

                                                                        <?php if (get_sub_field('icone')) : ?>

                                                                            <div class="col-3">

                                                                                <img src="<?php the_sub_field('icone'); ?>" class="img-fluid" alt="<?php the_sub_field('item'); ?>" title="<?php the_sub_field('item'); ?>" />

                                                                            </div>

                                                                        <?php endif ?>

                                                                    <?php else : ?>

                                                                        <?php // echo 'false'; 

                                                                        ?>

                                                                    <?php endif; ?>

                                                                <?php endwhile; ?>

                                                            <?php else : ?>

                                                                <?php // no rows found 

                                                                ?>

                                                            <?php endif; ?>

                                                        </div>

                                                    </div>

                                                </div><!-- end card -->

                                            </div>

                                        </a>





                                <?php $animate += 100;

                                        $count++;

                                    }

                                } else {

                                    // no posts found



                                }



                                /* Restore original Post Data */

                                wp_reset_postdata();



                                ?>

                            </div>

                        </div>

                <?php

                        $cont++;

                    endforeach;

                endif;

                ?>

            </div>



        </div><!-- end main row -->

    </div>

</section><!-- /.empreendimentos -->