<?php 

global $post;

 if(is_page('1324') || is_page('1338')):
    if(get_field('mostrar_banner', 15)):
        $class = 'icon_bar';
    else: 
        $hidden = 'd-none';
        $class = 'fixed-bottom nav_up bar_emp';
    endif;

else: 
    $class = ' ';
endif;
if (is_singular('empreendimentos')):
    $hidden_singular = 'd-none';
else:
    $hidden_singular = 'not-singular';

endif;
?> 


<div class="container-fluid h100 background_seventh banner_nav main_first <?php echo $hidden;?> <?php echo $hidden_singular;?>">
        <div class="d-none d-lg-flex row m-0 col-12 p-0 justify-content-center align-items-center">
            <col-4 class=" pr-3" data-aos="fade-up" data-aos-offset="0"
    data-aos-delay="50"
    data-aos-duration="400"
    data-aos-easing="ease-in-out"
    data-aos-once="false">
                <a href="/contato#v-pills-2" class="hvr-pulse-grow row p-3 w-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/vibrate.png' class='img-fluid' alt='' title='' lazy='loading' >
                    <p class="pl-3">Ligamos pra você</p>
                </a>
            </col-4>
            <col-4 class=" pr-3" data-aos="fade-up" data-aos-offset="0"
    data-aos-delay="50"
    data-aos-duration="400"
    data-aos-easing="ease-in-out"
    data-aos-once="false">
                <a href="/contato#v-pills-4" class="hvr-pulse-grow row p-3  w-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/phone (1).png' class='img-fluid' alt='' title='' lazy='loading'>
                    <p class="pl-3">Contato vendas</p>
                </a>
            </col-4>
            <col-4 class=" pr-3" data-aos="fade-up" data-aos-offset="0"
    data-aos-delay="50"
    data-aos-duration="400"
    data-aos-easing="ease-in-out"
    data-aos-once="false">
                <a id="chatblip" class="hvr-pulse-grow row p-3  w-100 m-0 justify-content-center align-items-center" href="#" target="_blank" rel="noopener" title="Abrir o chat">
                    <img class="chat_black" src='/wp-content/themes/quartzo/img/chat.png' class='img-fluid' alt='' title='' lazy='loading'>
                    <p class="pl-3">Chat online</p>
                </a>
            </col-4>
            <!-- <div class="whatsapp_btn" data-aos="flip-up" data-aos-duration="4000">
                <a href="https://wa.me/?text=Olá">
                    <i class="fa fa-whatsapp"></i>
                </a>
            </div> -->
        </div>
        <div class="d-lg-none row m-0 col-12 p-0 justify-content-center align-items-center">
            <div class="p-0">
                <a href="" class="hvr-pulse-grow row p-3 w-100 h-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/vibrate.png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <div class="p-0">
                <a href="" class="hvr-pulse-grow row p-3  w-100 h-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/phone (1).png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <div class="p-0">
                <a href="" class="hvr-pulse-grow row p-3  w-100 h-100 m-0 justify-content-center align-items-center">
                    <img class="chat_black"src='/wp-content/themes/quartzo/img/chat.png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <!-- <div class="whatsapp_btn m-0 p-0" >
                <a href="https://wa.me/?text=Olá">
                    <i class="fa fa-whatsapp"></i>
                </a>
            </div> -->
        </div>
    </div><!-- end container -->
    <div class="container-fluid <?php echo $class;?> h100 background_seventh banner_nav fixed-bottom ">
        <div class="d-lg-none row m-0 col-12 p-0 justify-content-center align-items-center">
            <div class="p-0">
                <a href="" class="hvr-pulse-grow row p-3 w-100 h-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/vibrate.png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <div class="p-0">
                <a href="" class="hvr-pulse-grow row p-3  w-100 h-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/phone (1).png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <div class="p-0">
                <a href="" class="hvr-pulse-grow row p-3  w-100 h-100 m-0 justify-content-center align-items-center">
                    <img class="chat_black"src='/wp-content/themes/quartzo/img/chat.png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <!-- <div class="whatsapp_btn m-0 p-0" >
                <a href="https://wa.me/?text=Olá">
                    <i class="fa fa-whatsapp"></i>
                </a>
            </div> -->
        </div>
    </div>
    <div class="container-fluid <?php echo $class;?> h100 background_seventh banner_nav fixed-bottom ">
        <div class="d-none d-lg-flex row m-0 col-12 p-0 justify-content-center align-items-center">
            <col-4 class=" pr-3" data-aos="fade-up" data-aos-offset="0"
    data-aos-delay="50"
    data-aos-duration="400"
    data-aos-easing="ease-in-out"
    data-aos-once="false">
                <a href="/contato#v-pills-2" class="hvr-pulse-grow row p-3 w-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/vibrate.png' class='img-fluid' alt='' title='' lazy='loading' >
                    <p class="pl-3">Ligamos pra você</p>
                </a>
            </col-4>
            <col-4 class=" pr-3" data-aos="fade-up" data-aos-offset="0"
    data-aos-delay="50"
    data-aos-duration="400"
    data-aos-easing="ease-in-out"
    data-aos-once="false">
                <a href="/contato#v-pills-4" class="hvr-pulse-grow row p-3  w-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/phone (1).png' class='img-fluid' alt='' title='' lazy='loading'>
                    <p class="pl-3">Contato vendas</p>
                </a>
            </col-4>
            <col-4 class=" pr-3" data-aos="fade-up" data-aos-offset="0"
    data-aos-delay="50"
    data-aos-duration="400"
    data-aos-easing="ease-in-out"
    data-aos-once="false">
    <a id="chatblip" class="hvr-pulse-grow row p-3  w-100 m-0 justify-content-center align-items-center" href="#" target="_blank" rel="noopener" title="Abrir o chat">
                    <img class="chat_black" src='/wp-content/themes/quartzo/img/chat.png' class='img-fluid' alt='' title='' lazy='loading'>
                    <p class="pl-3">Chat online</p>
                </a>
            </col-4>
            <!-- <col-4 class="pr-3 whatsapp_btn" data-aos="flip-up" data-aos-duration="4000">
                <a href="https://wa.me/?text=Olá">
                    <i class="fa fa-whatsapp"></i>
                </a>
            </div> -->
        </div>
        <div class="d-none row m-0 col-12 p-0 justify-content-center align-items-center">
            <div class="p-0">
                <a href="" class="hvr-pulse-grow row p-3 w-100 h-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/vibrate.png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <div class="p-0">
                <a href="" class="hvr-pulse-grow row p-3  w-100 h-100 m-0 justify-content-center align-items-center">
                    <img src='/wp-content/themes/quartzo/img/phone (1).png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <div class="p-0">
                <a class="hvr-pulse-grow row p-3  w-100 h-100 m-0 justify-content-center align-items-center" href="http://quartzo.housecrm.com.br/chat/?empreendimento=15902&amp;nome=&amp;email=&amp;ddd=&amp;telefone=&amp;pro=1&amp;filial=638&amp;source=direct&amp;media=&amp;campaign=&amp;referrer=&amp;keyword=&amp;host=localhost&amp;googleid=&amp;enterlink=http://localhost/300/quartzo-backend/empreendimento/portal-paradiso/&amp;informacao=undefined&amp;campanha=&amp;utmcontent=" target="_blank" rel="noopener" title="Abrir o chat">
                    <img class="chat_black"src='/wp-content/themes/quartzo/img/chat.png' class='img-fluid' alt='' title='' lazy='loading'>
                </a>
            </div>
            <!-- <div class="whatsapp_btn m-0 p-0">
                <a href="https://wa.me/?text=Olá">
                    <i class="fa fa-whatsapp"></i>
                </a>
            </div> -->
        </div>
    </div><!-- end container -->