<?php 
global $post;
$page_ID = $post->ID;
// get page ID

if ( is_front_page() || is_home() ) :
    $page_ID =  15;
    ?>
    <section class="banner_section pt-lg-0 " id="">
    <div class="container-fluid h-100 banner_container h-100 carousel_banner mb-0 p-0">
        <?php if ( have_rows( 'cadastro_de_sliders', $page_ID ) ) : ?>
        	<?php $count = 0; while ( have_rows( 'cadastro_de_sliders', $page_ID ) ) : the_row(); ?>
          
                <?php if ( get_sub_field( 'mostrar_na_home' ) == 1 ) : ?>
                    <?php $imagem_de_fundo = get_sub_field( 'imagem_de_fundo' ); ?>
                   
                    <div class="row item_<?php echo $count;?> justify-content-start align-items-center m-0 item mb-0 min-vh-100 mobile_too" style="background-image: url('<?php echo esc_url( $imagem_de_fundo['url'] ); ?>');"> 
                    <style>
                        .item_<?php echo $count;?> {
                            background-image: url('<?php echo esc_url( $imagem_de_fundo['url'] ); ?>') !important;
                        }
                    </style>
                        <div class="col-12 banner_content">
                            <div class="col-12 col-lg-6 p-0">
                                <h2 class="text-white" data-aos="fade-up"
        data-aos-offset="0"
        data-aos-delay="50"
        data-aos-duration="1000"
        data-aos-easing="ease-in-out"
        data-aos-mirror="true"
        data-aos-once="false"
        data-aos-anchor-placement="top-center">
                                    <?php the_sub_field( 'titulo' ); ?>
                                </h2>
                            </div>
                            <div class="w-100">
                            </div>
                            <div class="btn_banner col-12 col-lg-3 p-0 mt-5">
                                <?php $botao = get_sub_field( 'botao' ); ?>
                                <?php if ( $botao ) : ?>
                                    <a class="btn_second_text" href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>" data-aos="fade-down" data-aos-duration="8000">
                                        <div class="btn btn_second w-100">
                                            <?php echo esc_html( $botao['title'] ); ?>
                                        </div>

                                    </a>
                                <?php endif; ?>
                            
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            <?php $count++; endwhile; ?>
        <?php else : ?>
            <?php // no rows found ?>
        <?php endif; ?>
    </div><!-- end container -->
    <?php get_template_part( 'templates/global/template-part', 'barraicones' ); ?>

</section>
<?php 
else : ?>

<section class="banner_section pt-lg-0 " id="">
<div class="container-fluid h-100 banner_container h-100 carousel_banner mb-0 p-0">
    <?php if ( have_rows( 'cadastro_de_sliders', $page_ID ) ) : ?>
        <?php $count = 0; while ( have_rows( 'cadastro_de_sliders', $page_ID ) ) : the_row(); ?>
        
            <?php $imagem_de_fundo = get_sub_field( 'imagem_de_fundo' ); ?>
            <div class="row justify-content-start  item_<?php echo $count;?>  align-items-center m-0 item mb-0 min-vh-100" style="background-image: url('<?php echo esc_url( $imagem_de_fundo['url'] ); ?>');"> 
                <div class="col-12 banner_content">
                <style>
                        .item_<?php echo $count;?> {
                            background-image: url('<?php echo esc_url( $imagem_de_fundo['url'] ); ?>') !important;
                        }
                    </style>
                    <div class="col-12 col-lg-6 p-0">
                        <h2 class="text-white" data-aos="fade-up"
data-aos-offset="0"
data-aos-delay="50"
data-aos-duration="1000"
data-aos-easing="ease-in-out"
data-aos-mirror="true"
data-aos-once="false"
data-aos-anchor-placement="top-center">
                            <?php the_sub_field( 'titulo' ); ?>
                        </h2>
                    </div>
                    <div class="w-100">
                    </div>
                    <div class="btn_banner col-12 col-lg-3 p-0 mt-5">
                        <?php $botao = get_sub_field( 'botao' ); ?>
                        <?php if ( $botao ) : ?>
                            <a class="btn_second_text" href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>" data-aos="fade-down" data-aos-duration="8000">
                                <div class="btn btn_second w-100">
                                    <?php echo esc_html( $botao['title'] ); ?>
                                </div>

                            </a>
                        <?php endif; ?>
                       
                    </div>
                </div>
            </div>
            <?php $count++; endwhile; ?>
    <?php else : ?>
        <?php // no rows found ?>
    <?php endif; ?>
</div><!-- end container -->
<?php get_template_part( 'templates/global/template-part', 'barraicones' ); ?>

</section>
<?php endif;
?>
<!-- start breadcrumbs -->
<?php ah_breadcrumb(); ?>
<!-- end breadcrumbs -->