
<section class="testimonials" id=""> 
    <div class="container h-100">
        <div class="row justify-content-center align-items-stretch">
            <div class="col-md-8 text-center">
                <h2><?php the_field( 'titulo_avaliacao_dos_clientes', 'option' ); ?></h2>
                <h3><?php the_field( 'descricao_avaliacao_dos_clientes', 'option' ); ?></h3>
            </div>            
            <?php if ( have_rows( 'cadastro_de_avaliacao_dos_clientes', 'option' ) ) : ?>
                <div class="col-md-10 text-center carousel_testimonials">
                    <?php while ( have_rows( 'cadastro_de_avaliacao_dos_clientes', 'option' ) ) : the_row(); ?>
                        <div>
                            <div class="media row m-0 align-items-center">
                                <div class="col-md-3">
                                    <img class="img-fluid" src="<?php the_sub_field( 'foto' ); ?>" title="<?php the_sub_field( 'nome' ); ?>" alt="<?php the_sub_field( 'nome' ); ?>">
                                </div>
                                <div class="media-body col-md-9">
                                    <p>
                                        <?php the_sub_field( 'depoimento' ); ?>
                                    </p>
                                    <p><b><?php the_sub_field( 'nome' ); ?>, <?php the_sub_field( 'idade' ); ?> anos | 		<?php the_sub_field( 'cidade' ); ?> - 		<?php the_sub_field( 'estado' ); ?></b></p>
                                </div>
                            </div>
                        </div>
                        
                    <?php endwhile; ?>
                </div>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>  
        </div><!--/.container-->
    </div><!--/.row-->
</section><!--/.newsletter-->
