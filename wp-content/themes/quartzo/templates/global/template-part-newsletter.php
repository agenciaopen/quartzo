
<section class="newsletter" id=""> 
    <div class="container h-100">
        <div class="row justify-content-center align-items-stretch">
            <div class="col-md-8 text-center">
                <h2><?php the_field( 'titulo_newsletters', 'option' ); ?></h2>
                <h3><?php the_field( 'descricao_newsletter', 'option' ); ?></h3>
                <?php the_field( 'formulario_newsletter', 'option' ); ?>
            </div>            
        </div><!--/.container-->
    </div><!--/.row-->
</section><!--/.newsletter-->
