<?php
global $post;
$page_ID = $post->ID;


?>
    <?php if ( get_field( 'desativar_tour_virtual',  $page_ID ) == 0 ) : ?>
        <?php if (get_field( 'video_tour_virtual',  $page_ID )): ?>
            <section class="video d-none d-lg-block background_seventh tour">
                <div class="container h-100 " id="tour">
                    <div class="row h-100 align-items-center img_360 justify-content-center">
                        <div class="col-lg-12 text-center">
                            <div class="column justify-content-start align-items-center">
                                <div class="col-4 text-center">
                                    <img src="/wp-content/themes/quartzo/img/Grupo 2.png" class="img-fluid" alt="empreendimento">
                                    <p class="text-center text-white mt-3">Tour Virtual</p>
                                    <div class="btn_mcmv text-center pl-5  mt-3">
                                    <a data-fancybox data-width="640" data-height="360" href="<?php the_field( 'video_tour_virtual', $page_ID ); ?>" class="btn_second_text">
                                                                <div class="btn btn_second">quero saber mais </div>
                                                            </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
         </section><!-- /.mcmv -->
        <?php endif;?>
<?php endif;?>
<?php if ( get_field( 'desativar_video_',  $page_ID ) == 0 ) : ?>
    <?php if (get_field( 'video',  $page_ID )): ?>
        <section class="video d-none d-lg-block background_seventh videobg">
            <div class="container h-100 mt-5 video_empreendimento pb-5" id="video">
                <div class="row h-100 align-items-center justify-content-center">
                    <div class="col-lg-12 text-center">
                        <h2 class="text-center">Vídeo</h2>
                        <hr class="hr_bot">
                    </div>
                    <div class="col-lg-8 text-center tint mb-5">
                        <a data-fancybox data-width="640" data-height="360" href="<?php the_field( 'video',  $page_ID ); ?>" class='d-flex flex-column'>
                            <img src='/wp-content/themes/quartzo/img/play-button.png' class=' play' alt='' title='' lazy='loading'>
                            <?php if ( get_field( 'imagem' ) ) : ?>
                                <img src="<?php the_field( 'imagem' ); ?>" class='img-fluid thumbnail'/>
                            <?php else : 
                                $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');
                                ?>
                            
                                <img src='<?php echo $url;?>' class='img-fluid thumbnail' alt='' title='' lazy='loading'>
                            <?php endif ?>
                        </a>
                    </div>
                </div>
            </div>
        </section><!-- /.mcmv -->
    <?php endif;?>
<?php endif;?>

