<section class="lancamentos d-none d-lg-block">

    <div class="container h-100 ">

        <div class="row h-100 align-items-center justify-content-between">

            <div class=" col-lg-7 text_description ">

                <div class="col-12 row justify-content-start aling-items-strech m-0 p-0">

                    <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->

                    <div class=" col-lg-1 hr-left"></div>

                    <h1 class="col-lg-11 pr-0"><?php the_field( 'titulo' ); ?>

</h1>

                </div><!-- /.col-12 row justify-content-start aling-items-strech -->

                <p class="mt-3">

                <?php the_field( 'descricao' ); ?>

                <div class="btn_mcmv mt-4">

                    <a class="btn_second_text" href="https://api.whatsapp.com/send?phone=5531994262983" target="_blank" role="button">

                        <div class="btn btn_second">Falar com um consultor</div>

                    </a>

                </div>

            </div><!-- /.col-lg-6 text_description -->

            <div class="col-12 col-lg-5 text-center">

            <?php if ( get_field( 'imagem' ) ) : ?>

	<img src="<?php the_field( 'imagem' ); ?>" class='img-fluid' alt='mcmv' title='mcmv' lazy='loading'/>

<?php else : ?>

                <img src='/wp-content/themes/quartzo/img/Quartzo Alto da Mata - Torres.png' class='img-fluid' alt='mcmv' title='mcmv' lazy='loading'>

<?php endif; ?>



            </div>

        </div>

    </div>

</section><!-- /.mcmv -->