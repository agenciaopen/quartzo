<?php if ( get_field( 'video_tour_virtual' ) ) : ?>
    <section class="singleCollapseTour d-block d-lg-none py-0 mb-3">
        <div class="container h-100 ">
            <div class="row h-100 align-items-start justify-content-start m-0 ">
                <div id="accordion" class="w-100">
                    <div class="card">
                        <div class="card-header background_seventh w-100" id="headingTour">
                            <h5 class="w-100">
                                <button class="d-flex align-items-center justify-content-start btn btn-link btn_single background_seventh w-100 p-3" data-toggle="collapse" data-target="#collapseTour" aria-expanded="true" aria-controls="collapseTour">
                                    <img src='/wp-content/themes/quartzo/img/Grupo 2.png' class='img-fluid' alt='' title='' lazy='loading'>
                                    Tour Virtual
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTour" class="collapse" aria-labelledby="headingTour" data-parent="#accordion">
                            <div class="card-body">
                                <div class="col-12 text-center img_360 d-flex align-items-center justify-content-start">
                                    <div class="col-7">
                                        <div class="col-12 p-0 text-center">
                                            <img src="/wp-content/themes/quartzo/img/Grupo 2.png" class="img-fluid" alt="empreendimento">
                                            <p class="text-center text-white mt-3">Tour Virtual</p>
                                            <div class="btn_mcmv text-center  mt-3">
                                                <a data-fancybox data-width="640" data-height="360" href="<?php the_field( 'video_tour_virtual' ); ?>" class="btn_second_text">
                                                    <div class="btn btn_second">quero saber mais </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- main row -->
    </section><!-- /.status -->
<?php endif ?>