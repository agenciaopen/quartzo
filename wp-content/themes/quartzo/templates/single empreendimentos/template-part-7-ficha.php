<?php if ( have_rows( 'cadastro_de_diferenciais' ) ) : ?>
    <svg xmlns="http://www.w3.org/2000/svg" width="660.387" height="720.68" viewBox="0 0 660.387 720.68" class="svg_first rightsvg">
  <path id="Caminho_1301" data-name="Caminho 1301" d="M2272.427,1094l-57.436-34.459V942.006l57.436-34.461v-4.657L2213,938.547l-95.947-57.567,96.973-58.184a1.993,1.993,0,0,0,.97-1.711V704.819l57.436,34.462v-4.656l-55.552-33.332,55.552-33.332V663.3l-57.436,34.462V582.632l57.436-34.462v-4.656L2213,579.173l-95.947-57.567,96.973-58.184a2,2,0,0,0,.97-1.712V401.88H2211v58.7l-97.83,58.7-95.947-57.567,96.974-58.184,2.742-1.646h-7.76l-93.839,56.3c-21.069-11.292-5.876,3.526-5.876,3.526l-96.973,58.184-99.826,59.9a1.992,1.992,0,0,0-.97,1.712V697.767c-.247,11.144,3.785,1.613,2,.425.718.562-.524.688-3.88,3.1l-96.973,58.184-.018.013-99.809,59.886a2,2,0,0,0-.969,1.711V940.878a2,2,0,1,0,3.993,0V822.217l97.83-58.7,97.83,58.7V937.35l-96.8-58.081a2,2,0,1,0-2.053,3.423l96.976,58.186-96.976,58.185a2,2,0,0,0,2.053,3.424l99.827-59.9.013-.009,98.786-59.273,95.947,57.569-96.973,58.183a2,2,0,0,0,2.054,3.424l96.8-58.082v116.266a1.991,1.991,0,0,0,.97,1.711l99.826,59.894c.016.009.032.012.047.021a2.059,2.059,0,0,0,.4.171c.031.009.059.02.09.028a1.986,1.986,0,0,0,.489.067,2.011,2.011,0,0,0,.476-.063c.025-.006.049-.016.074-.023a2.073,2.073,0,0,0,.366-.146c.032-.016.061-.033.092-.05a2.007,2.007,0,0,0,.345-.255c.011-.01.024-.018.034-.028a2.033,2.033,0,0,0,.3-.381.318.318,0,0,0,.02-.027.776.776,0,0,0,.043-.091,1.8,1.8,0,0,0,.107-.224c.021-.056.037-.113.053-.17a2.034,2.034,0,0,0,.048-.2c.012-.073.018-.145.023-.219,0-.04.012-.079.012-.12V1000.772a2,2,0,0,0-.97-1.712l-96.974-58.183,95.947-57.569,97.83,58.7v118.662a.4.4,0,0,0,.006.048,1.71,1.71,0,0,0,.021.226c.008.056.013.113.026.168a1.7,1.7,0,0,0,.061.2c.019.057.035.115.059.172s.065.119.1.179a1.426,1.426,0,0,0,.09.152,1.568,1.568,0,0,0,.128.157c.042.047.079.1.124.138s.094.078.141.117.112.09.173.129c.014.009.026.021.04.029l60.46,36.275ZM1815.685,582.632l95.834-57.5V640.267l-95.834,57.5Zm-2,236.126L1717.74,761.19l95.949-57.569,95.946,57.568Zm97.833,61.092-95.834,57.5V822.215l95.834-57.5Zm-93.95-178.557,95.946-57.568,95.947,57.568-95.947,57.567ZM2011.345,937.35l-95.833-57.5V764.715l95.833,57.5Zm0-119.791-93.95-56.37,93.95-56.37Zm0-119.792-95.833-57.5V525.132l95.833,57.5ZM1917.4,521.606l95.946-57.567,95.947,57.567-95.947,57.567Zm193.776,3.526V640.267l-95.833,57.5V582.632Zm0,476.77v115.133l-95.833-57.5V944.4Zm-95.833-64.552V824.61l93.95,56.37Zm95.833-59.9-95.833-57.5V704.819l95.833,57.5Zm-93.95-176.161,96.974-58.184a2,2,0,0,0,.97-1.711V525.132l95.833,57.5V700.163l-97.83,58.7Zm97.944,61.026,95.833-57.5V819.955l-95.833,57.5Z" transform="translate(-1612.04 -401.88)" fill="#ee4c4c" opacity="0.29"/>
</svg>
    <section class="diferenciais page2">
        <div class="container h-100 ">
            <div class="column h-100 align-items-center justify-content-center">
                <div class="col-lg-10 row">
                    <div class=" col-lg-2 hr-left"></div>
                    <?php if (get_field( 'titulo_diferenciais' )): ?>
                        <h2 class="col-10"><?php the_field( 'titulo_diferenciais' ); ?></h2>
                    <?php else : ?>
                        <h2 class="col-10">Diferenciais</h2>
                    <?php endif;?>
                </div><!-- /.col-lg-10 -->
                <div class="row m-0 mt-4">
                    <?php while ( have_rows( 'cadastro_de_diferenciais' ) ) : the_row(); ?>
                        <div class="col-md-4 mb-4 text-center">
                            <h3 class="mt-3"><b><?php the_sub_field( 'titulo' ); ?></b></h3>
                            <?php if ( get_sub_field( 'imagem' ) ) : ?>
                                <img src="<?php the_sub_field( 'imagem' ); ?>" class="img-fluid mt-3 mb-2"/>
                            <?php endif ?>
                            <p><small><?php the_sub_field( 'descricao' ); ?></small></p>
                        </div>
                        
                    <?php endwhile; ?>
                </div>
                    
                
        </div>
    </section><!-- /.status -->
<?php else : ?>
    <?php // no rows found ?>
<?php endif; ?>

<section class="ficha d-none d-lg-block background_eleventh" id="ficha">
    <div class="container h-100 ">
        <div class="column h-100 align-items-center justify-content-center">
            <div class="col-lg-10 row">
                <div class=" col-lg-2 hr_left_white"></div>
                <?php if (get_field( 'ficha_tecnica_titulo' )): ?>
                                        <h2 class="col-10"><?php the_field( 'ficha_tecnica_titulo' ); ?></h2>
                                    <?php else : ?>
                                        <h2 class="col-lg-10">Ficha Técnica</h2>
                                    <?php endif;?>
            </div><!-- /.col-lg-10 -->
            <div class="column col-lg-10">
            <?php if ( have_rows( 'cadastro_de_itens_ficha_tecnica_' ) ) : ?>
                                        <ul class="lista_ficha">
                                            <?php while ( have_rows( 'cadastro_de_itens_ficha_tecnica_' ) ) : the_row(); ?>
                                            <li class="item_ficha bullet">
                                                <p class="ficha_text">
                                                    <?php the_sub_field( 'item' ); ?>
                                                </p>
                                            </li>
                                            <?php endwhile; ?>
                                        </ul>
                                    <?php else : ?>
                                        <?php // no rows found ?>
                                    <?php endif; ?>
            </div>
        </div>
    </div>
</section><!-- /.mcmv -->