<section class="singleCollapseFicha d-block d-lg-none py-0 mb-3">
    <div class="container h-100 ">
        <div class="row h-100 align-items-start justify-content-start m-0">
            <div id="accordion" class="w-100">
                <div class="card">
                    <div class="card-header background_seventh w-100" id="headingFicha">
                        <h5 class="w-100">
                            <button class="d-flex align-items-center justify-content-start btn btn-link btn_single background_seventh w-100 p-3" data-toggle="collapse" data-target="#collapseFicha" aria-expanded="true" aria-controls="collapseFicha">
                                <img src='/wp-content/themes/quartzo/img/noun_Document_3455449.png' class='img-fluid' alt='' title='' lazy='loading'>
                                Ficha Técnica
                            </button>
                        </h5>
                    </div>
                    

                    <div id="collapseFicha" class="collapse" aria-labelledby="headingFicha" data-parent="#accordion">
                        <div class="card-body background_eleventh">
                            <div class="h-100 align-items-center justify-content-center">
                                <div class="col-lg-10 row">
                                    <div class=" col-lg-2 hr_left_white"></div>
                                    <?php if (get_field( 'ficha_tecnica_titulo' )): ?>
                                        <h2 class="col-10"><?php the_field( 'ficha_tecnica_titulo' ); ?></h2>
                                    <?php else : ?>
                                        <h2 class="col-lg-10">Ficha Técnica</h2>
                                    <?php endif;?>
                                </div><!-- /.col-lg-10 -->
                                <div class="column col-lg-10">
                                    <?php if ( have_rows( 'cadastro_de_itens_ficha_tecnica_' ) ) : ?>
                                        <ul class="lista_ficha">
                                            <?php while ( have_rows( 'cadastro_de_itens_ficha_tecnica_' ) ) : the_row(); ?>
                                            <li class="item_ficha bullet">
                                                <p class="ficha_text">
                                                    <?php the_sub_field( 'item' ); ?>
                                                </p>
                                            </li>
                                            <?php endwhile; ?>
                                        </ul>
                                    <?php else : ?>
                                        <?php // no rows found ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- main row -->
    </div>
</section><!-- /.status -->
<style>
iframe{
    width: 100%;
}
@media screen and (max-width: 767px){
    .ficha .bullet::before, .singleCollapseFicha .bullet::before {
        content: "• ";
        color: #fff;
        float: left;
        margin-right: 1rem;
        margin-top: -3px;
        x: ;
    }
}
</style>