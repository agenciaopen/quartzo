<section class="ape pb-0">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-9">
                <h2 class="text-center"><?php the_field( 'conheca_o_seu_novo_ape' ); ?></h2>
                <hr class="hr_bot">
            </div>
            <div class="w-100"></div>
            <div class="col-10">
                <p class="text-center">
                <?php the_field( 'conheca_o_seu_novo_ape_descricao', false, false ); ?>
                </p>
            </div><!-- end title and text -->
        </div><!-- end row -->
    </div>
</section><!-- /.duvidas -->