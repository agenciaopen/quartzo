<?php
global $post;
$page_ID = $post->ID;


?>
<section class="planta d-none d-lg-block" id="planta">
    <div class="container h-100" id="plantaimovel">
        <div class="row h-100 align-items-center justify-content-between">
            <div class="col-12 row justify-content-start aling-items-strech">
                <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->
                <div class=" col-lg-1 hr-left"></div>
                <?php if (get_field( 'planta_do_imovel_titulo' )): ?>
                    <h2 class="col-lg-11"><?php the_field( 'planta_do_imovel_titulo' ); ?></h2>
                                        <?php else : ?>
                                            <h2 class="col-lg-11">Planta do Imóvel</h2>
                                        <?php endif;?>
            </div><!-- /.col-12 row justify-content-start aling-items-strech -->
            <div class="col-lg-12">
                <p class="">
                <?php if (get_field( 'planta_do_imovel_descricao' )): ?>
                                                <?php the_field( 'planta_do_imovel_descricao' ); ?>
                                            <?php else : ?>
                                            <?php endif;?>
                </p>
            </div><!-- end title and text -->
            <div class="slider-for col-lg-5">
                <?php while ( have_rows( 'planta_do_imovel_galeria' ) ) : the_row(); ?>
                    <?php $planta_do_imovel_imagem_ = get_sub_field( 'planta_do_imovel_-_imagem_' ); ?>
                    <?php if ( $planta_do_imovel_imagem_ ) : ?>
                        <div class="planta_image" style="background-image: url('<?php echo esc_url( $planta_do_imovel_imagem_['url'] ); ?>')">
                            <a  class="col-lg-4 p-0" href="<?php echo esc_url( $planta_do_imovel_imagem_['url'] ); ?>" data-fancybox="planta" data-caption="<?php the_sub_field( 'planta_do_imovel_titulo' ); ?>">
                                <img class="col-lg-12 p-0 d-none" src="<?php echo esc_url( $planta_do_imovel_imagem_['url'] ); ?>" alt="" />
                                <div class="hover_planta">
                                    <div class="hover_planta_text d-flex flex-column justify-content-center align-items-center">
                                        <img src='/wp-content/themes/quartzo/img/resize-1.png' class='img-fluid mb-4' alt='' title='' lazy='loading'>
                                        <?php the_sub_field( 'planta_do_imovel_titulo' ); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                                                    
                    <?php endif; ?>
                                                    
                <?php endwhile; ?>
                
            </div><!-- end slider-for -->
            <div class="col-lg-6 m-0 h-100 carousel_planta align-items-center justify-content-center">
            <?php if ( have_rows( 'planta_do_imovel_galeria' ) ) : ?>

                        <?php while ( have_rows( 'planta_do_imovel_galeria' ) ) : the_row(); ?>
                            <?php $planta_do_imovel_imagem_ = get_sub_field( 'planta_do_imovel_-_imagem_' ); ?>
                            <?php if ( $planta_do_imovel_imagem_ ) : ?>
                                <a class="p-0" href="<?php echo esc_url( $planta_do_imovel_imagem_['url'] ); ?>" data-fancybox="ape" data-caption="<?php the_sub_field( 'planta_do_imovel_titulo' ); ?>">
                                    <img class="col-lg-12 p-0" src="<?php echo esc_url( $planta_do_imovel_imagem_['url'] ); ?>" alt="<?php echo esc_attr( $planta_do_imovel_imagem_['alt'] ); ?>" />
                                </a>
                            <?php endif; ?>
                            
                        <?php endwhile; ?>
                        <?php endif; ?>

            </div>
            <div class="col-md-12">
                <p>Baixar plantas do imóvel</p>
                <?php if ( have_rows( 'planta_do_imovel_galeria_copiar', $page_ID ) ) : ?>
                    <?php while ( have_rows( 'planta_do_imovel_galeria_copiar', $page_ID ) ) : the_row(); ?>
                        <?php $planta_do_imovel_arquivo = get_sub_field( 'planta_do_imovel_-_arquivo' ); ?>
                        <?php if ( $planta_do_imovel_arquivo ) : ?>
                            <a href="<?php echo esc_url( $planta_do_imovel_arquivo['url'] ); ?>" download>
                                <?php the_sub_field( 'planta_do_imovel_titulo_arquivo' ); ?>
                            </a>
                        <?php endif; ?>
                        
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section><!-- /.duvidas -->