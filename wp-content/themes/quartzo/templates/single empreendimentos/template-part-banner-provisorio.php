<?php


get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

  <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');
                                        if ( $url ) :
                                            $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');
                                        else :
                                            $url = get_field('imagem', $page_ID);
                                        endif;
                                    ?>
<section class="banner_section_prov pb-0 py-lg-0 background_seventh" id="" >
    <div class="container-fluid h-100 banner_container carousel_banner h-100 mb-0 p-0">
        <div class="row justify-content-start align-items-center m-0 item mb-0 emp" style="background-image: url('<?php echo $url;?>');">
            <style>
                .item{
                    background-image: url('<?php echo $url;?>') !important;
                }
            </style>
            <div class="col-12 banner_content_prov">
                <div class="col-12 col-lg-6 p-0">
                <?php
                        $terms = get_the_terms($post->ID, 'status');
                            if( $terms):
                                foreach( $terms as $t ):
                        ?>
                        
									<?php
									$taxonomy_prefix = 'status';
									$term_id = $t->term_id ;
									$term_id_prefixed = $taxonomy_prefix .'_'. $term_id;

									?>
									                    <h2 class="banner_text_small mb-3"><?php echo $t->name;?></h2>

                        <?php
                                endforeach;
                            endif;
                        ?>
                    <h2 class="text-white">
                        <?php the_title();?>
                    </h2>
                    <div class="btn_banner col-12 col-lg-5 p-0 mt-5">
                        <a class="btn_second_text" href="https://api.whatsapp.com/send?phone=5531994262983" target="_blank" role="button">
                            <div class="btn btn_second w-100">
                                quero meu apê
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end container -->
    <div class="container background_seventh banner_nav_interna menuinterna">
        <div class="d-none d-lg-flex row m-0 col-12 p-0 justify-content-center align-items-center ">
            <?php if ( get_field( 'desativar_conheca_o_seu_novo_ape', $page_ID  ) == 0 ) : ?>
                <a href="#galeria" class="d-flex align-items-center justify-content-center nav_link_interna col-2">
                    <p class="nav_text_interna">Galeria de imagens</p>
                </a>
            <?php endif;?>
            <?php if ( get_field( 'desativar_planta_do_imovel', $page_ID  ) == 0 ) : ?>
                <a href="#plantaimovel" class="d-flex align-items-center justify-content-center nav_link_interna col-2">
                    <p class="nav_text_interna">Planta do Imóvel</p>
                </a>
            <?php endif;?>
            <?php if ( get_field( 'desativar_tour_virtual',  $page_ID ) == 0 ) : ?>
                <?php if (get_field( 'video_tour_virtual',  $page_ID )): ?>
                    <a href="#tour" class="d-flex align-items-center justify-content-center nav_link_interna col-2">
                        <p class=" nav_text_interna">Tour Virtual</p>
                    </a>
                <?php endif;?>
            <?php endif;?>
            <?php if ( get_field( 'desativar_mapa',  $page_ID ) == 0 ) : ?>
                <?php if (get_field( 'iframe_mapa' )): ?>
                    <a href="#mapa" class="d-flex align-items-center justify-content-center nav_link_interna col-2">
                        <p class="nav_text_interna">Localização</p>
                    </a>
                <?php endif;?>
            <?php endif;?>
            <?php if ( get_field( 'desativar_video_',  $page_ID ) == 0 ) : ?>
                <?php if (get_field( 'video',  $page_ID )): ?>
                    <a href="#video" class="d-flex align-items-center justify-content-center nav_link_interna col-2">
                        <p class="nav_text_interna">Vídeo</p>
                    </a>
                <?php endif;?>
            <?php endif;?>
            <?php if ( get_field( 'desativar_ficha_tecnica',  $page_ID ) == 0 ) : ?>
                <a href="#ficha" class="d-flex align-items-center justify-content-center nav_link_interna col-2">
                    <p class="nav_text_interna">Ficha Técnica</p>
                </a>
            <?php endif;?>
        </div>
    </div>
    <?php get_template_part( 'templates/global/template-part', 'barraicones' ); ?>

</section>
<!-- start breadcrumbs -->
<?php ah_breadcrumb(); ?>
<!-- end breadcrumbs -->