<?php if ( have_rows( 'planta_do_imovel_galeria' ) ) : ?>
    <section class="singleCollapsePlanta d-block d-lg-none py-0 my-3">
        <div class="container h-100 ">
            <div class="row h-100 align-items-start justify-content-start m-0 ">
                <div id="accordion" class="w-100">
                    <div class="card">
                        <div class="card-header background_seventh w-100" id="headingPlanta">
                            <h5 class="w-100">
                                <button class="d-flex align-items-center justify-content-start btn btn-link btn_single background_seventh w-100 p-3"  data-toggle="collapse" data-target="#collapsePlanta" aria-expanded="true" aria-controls="collapsePlanta">
                                <img src='/wp-content/themes/quartzo/img/Grupo 2056.png' class='img-fluid' alt='' title='' lazy='loading'>    
                                Planta do imóvel
                                </button>
                            </h5>
                        </div>
                        <div id="collapsePlanta" class="collapse" aria-labelledby="headingPlanta" data-parent="#accordion">
                            <div class="card-body">
                                <div class="m-0 h-100 align-items-center justify-content-between">
                                    <div class="col-12 row m-0 justify-content-start aling-items-strech">
                                        <div class=" col-2 hr-left"></div>
                                        <?php if (get_field( 'planta_do_imovel_titulo' )): ?>
                                            <h2 class="col-10"><?php the_field( 'planta_do_imovel_titulo' ); ?></h2>
                                        <?php else : ?>
                                            <h2 class="col-10">Planta do Imóvel</h2>
                                        <?php endif;?>
                                    </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                                    <div class="col-12">
                                            
                                        <p class="my-3">
                                            <?php if (get_field( 'planta_do_imovel_descricao' )): ?>
                                                <?php the_field( 'planta_do_imovel_descricao' ); ?>
                                            <?php else : ?>
                                            <?php endif;?>
                                        </p>
                                    </div><!-- end title and text -->
                                    <div class=" m-0 h-100 carousel_planta_mobile">
                                            <?php while ( have_rows( 'planta_do_imovel_galeria' ) ) : the_row(); ?>
                                                <?php $planta_do_imovel_imagem_ = get_sub_field( 'planta_do_imovel_-_imagem_' ); ?>
                                                <?php if ( $planta_do_imovel_imagem_ ) : ?>
                                                    <a class="p-0" href="<?php echo esc_url( $planta_do_imovel_imagem_['url'] ); ?>" data-fancybox="ape" data-caption="<?php the_sub_field( 'planta_do_imovel_titulo' ); ?>">
                                                        <img class="col-lg-12 p-0" src="<?php echo esc_url( $planta_do_imovel_imagem_['url'] ); ?>" alt="<?php echo esc_attr( $planta_do_imovel_imagem_['alt'] ); ?>" />
                                                    </a>
                                                <?php endif; ?>
                                                
                                            <?php endwhile; ?>
                                       
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- main row -->
        </div>
    </section><!-- /.status -->
    <?php else : ?>
                                            <?php // no rows found ?>
                                        <?php endif; ?>