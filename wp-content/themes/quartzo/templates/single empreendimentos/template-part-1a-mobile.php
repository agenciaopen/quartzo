<section class="lancamentosMobile d-block d-lg-none pb-0">
    <div class="container h-100 ">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12 p-0 text-center">
            <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');
                                        if ( $url ) :
                                            $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');
                                        else :
                                            $url = get_field('imagem', $page_ID);
                                        endif;
                                    ?>
                <img src='<?php echo $url;?>' class='img-fluid' alt='mcmv' title='mcmv' lazy='loading'>
            </div>
            <div class=" col-12 col-lg-6 text_description mt-3 ">
                <div class="row justify-content-center">
                    <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->
                    <div class="col-2 col-lg-1 hr-left"></div>
                    <h2 class="col-10 col-lg-11"><?php the_field( 'titulo' ); ?></h2>
                </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                <p class="mt-3">
                <?php the_field( 'descricao' ); ?>
                <div class="btn_mcmv text-center mt-5">
                    <a class="btn_second_text" href="#" role="button">
                        <div class="btn btn_second">Falar com um consultor</div>
                    </a>
                </div>
            </div><!-- /.col-lg-6 text_description -->
        </div>
    </div>
</section><!-- /.mcmv -->