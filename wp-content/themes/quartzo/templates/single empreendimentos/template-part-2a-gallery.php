<section class="gallery d-none d-md-flex" id="galeria">
    <div class="container-fluid h-100 px-0">
        <div class=" m-0 h-100 carousel_ape_desk align-items-center justify-content-start row">
            <?php if ( have_rows( 'conheca_o_seu_novo_ape_galeria' ) ) : ?>
            <?php while ( have_rows( 'conheca_o_seu_novo_ape_galeria' ) ) : the_row(); ?>
                
                
                <a style="background-image: url('<?php the_sub_field( 'imagem' ); ?>')" class="col-lg-3 p-0" href="<?php the_sub_field( 'imagem' ); ?>" data-fancybox="ape" data-caption="<?php the_sub_field( 'titulo' ); ?>">
                <img class="col-lg-12 p-0 img-fluid d-none" src="<?php the_sub_field( 'imagem' ); ?>" alt="" />
                <div class="hover_ape">
                    <div class="hover_ape_text d-flex flex-column justify-content-start align-items-center">
                        <img src='/wp-content/themes/quartzo/img/resize-1.png' class='img-fluid mb-4' alt='' title='' lazy='loading'>
                        <?php the_sub_field( 'titulo' ); ?>
                        
                    </div>
                </div>
            </a>
            <?php endwhile; ?>
    <?php else : ?>
        <?php // no rows found ?>
    <?php endif; ?>
            
        </div>
    </div>
</section><!-- /.duvidas -->

<section class="gallery d-md-none" id="galeria">
    <div class="container-fluid h-100 px-0">
        <div class=" m-0 h-100 carousel_ape align-items-center justify-content-center">
            <?php if ( have_rows( 'conheca_o_seu_novo_ape_galeria' ) ) : ?>
            <?php while ( have_rows( 'conheca_o_seu_novo_ape_galeria' ) ) : the_row(); ?>
                
                
                <a class="col-lg-12 p-0" href="<?php the_sub_field( 'imagem' ); ?>" data-fancybox="ape" data-caption="<?php the_sub_field( 'titulo' ); ?>">
                <img class="col-lg-12 p-0 img-fluid" src="<?php the_sub_field( 'imagem' ); ?>" alt="" />
                <div class="hover_ape">
                    <div class="hover_ape_text d-flex flex-column justify-content-center align-items-center">
                        <img src='/wp-content/themes/quartzo/img/resize-1.png' class='img-fluid mb-4' alt='' title='' lazy='loading'>
                        <?php the_sub_field( 'titulo' ); ?>
                        
                    </div>
                </div>
            </a>
            <?php endwhile; ?>
    <?php else : ?>
        <?php // no rows found ?>
    <?php endif; ?>
            
        </div>
    </div>
</section><!-- /.duvidas -->

