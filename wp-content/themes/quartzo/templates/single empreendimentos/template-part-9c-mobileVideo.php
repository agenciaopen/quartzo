<section class="singleCollapseVideo d-block d-lg-none py-0 mb-3">
    <div class="container h-100 ">
        <div class="row h-100 align-items-start justify-content-start m-0 ">
            <div id="accordion" class="w-100">
                <div class="card">
                    <div class="card-header background_seventh w-100" id="headingVideo">
                        <h5 class="w-100">
                            <button class="d-flex align-items-center justify-content-start btn btn-link btn_single background_seventh w-100 p-3" data-toggle="collapse" data-target="#collapseVideo" aria-expanded="true" aria-controls="collapseVideo">
                                <img src='/wp-content/themes/quartzo/img/noun_Video_1591055.png' class='img-fluid' alt='' title='' lazy='loading'>
                                Video
                            </button>
                        </h5>
                    </div>
                    <div id="collapseVideo" class="collapse" aria-labelledby="headingVideo" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row h-100 align-items-center justify-content-center">
                                <div class="col-lg-12 text-center">
                                    <h2 class="text-center">Vídeo</h2>
                                    <hr class="hr_bot">
                                </div>
                                <div class="col-lg-8 text-center tint ">
                                    <a data-fancybox data-width="640" data-height="360" href="<?php the_field( 'video' ); ?>" class='d-flex flex-column'>
                                        <img src='/wp-content/themes/quartzo/img/play-button.png' class=' play' alt='' title='' lazy='loading'>
                                        <?php if ( get_field( 'imagem' ) ) : ?>
                                            <img src="<?php the_field( 'imagem' ); ?>" class='img-fluid thumbnail'/>
                                        <?php else : 
                                            $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');
                                        endif;
                                            ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- main row -->
    </div>
</section><!-- /.status -->