<section class="singleCollapseStatus d-block d-lg-none py-0 mb-3">
    <div class="container h-100 ">
        <div class="row h-100 align-items-start justify-content-start m-0 background_seventh">
            <div id="accordion" class="w-100">
                <div class="card">
                    <div class="card-header background_seventh w-100" id="headingStatus">
                        <h5 class="w-100">
                            <button class="d-flex align-items-center justify-content-start btn btn-link btn_single background_seventh w-100 p-3" data-toggle="collapse" data-target="#collapseStatus" aria-expanded="true" aria-controls="collapseStatus">
                                <img src='/wp-content/themes/quartzo/img/noun_Evolution_3410769.png' class='img-fluid' alt='' title='' lazy='loading'>
                                Status do empreendimento
                            </button>
                        </h5>
                    </div>
                    <div id="collapseStatus" class="collapse" aria-labelledby="headingStatus" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row h-100 align-items-start justify-content-start">
                            <div class=" col-lg-12">
                                    <div class="row justify-content-start aling-items-strech m-0">
                                        <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->
                                        <div class=" col-2 hr-left"></div>
                                        <?php if (get_field( 'status_do_empreendimento_titulo' )): ?>
                                            <h2 class="col-10"><?php the_field( 'status_do_empreendimento_titulo' ); ?></h2>
                                        <?php else : ?>
                                            <h2 class="col-10">Status do empreendimento</h2>
                                        <?php endif;?>
                                    </div><!-- /.col-12 row justify-content-start aling-items-strech -->
                                    <p class="mt-3 mb-5">
                                        <?php if (get_field( 'descricao_status_do_empreendimento' )): ?>
                                            <?php the_field( 'descricao_status_do_empreendimento' ); ?>
                                        <?php else : ?>
                                            Veja quanto falta para o seu futuro apartamento ficar pronto!
                                        <?php endif;?>
                                    </p>
                                </div>
                                


                                <div class="col-lg-4">
                                    <!-- <div class="column align-items-start justify-content-start"> -->
                                    <p class='mb-3'>Previsão:</p>
                                    <div class="row justify-content-start align-items-center m-0 mb-3">
                                        <div class="red_circle d-flex justify-content-center align-items-center mr-2">
                                            <img class="img-fluid " src="/wp-content/themes/quartzo/img/noun_brick_3104490 (1).png" alt="">
                                        </div>
                                        <p>Início: <?php the_field( 'previsao' ); ?></p>
                                    </div>
                                    <div class="row justify-content-start align-items-center m-0 mb-3">
                                        <div class="red_circle d-flex justify-content-center align-items-center mr-2">
                                            <img class="img-fluid " src="/wp-content/themes/quartzo/img/noun_building_179144.png" alt="">
                                        </div>
                                        <p>Fim: <?php the_field( 'fim' ); ?></p>
                                    </div>
                                    <!-- </div>/.column -->
                                </div>
                                <div class="col-lg-8">
                                    <div class="column align-items-between justify-content-start">
                                       
                                        <?php if ( have_rows( 'cadastro_de_status' ) ) : ?>
                                            <div class="row justify-content-between align-items-start">
                                            <?php while ( have_rows( 'cadastro_de_status' ) ) : the_row(); ?>
                            <?php if(get_sub_field( 'terraplanagem' )): ?>
                                <div class="col-lg-6">
                                    <div class="row justify-content-between align-items-center m-0">
                                        <p>Terraplanagem </p>
                                        <p><?php the_sub_field( 'terraplanagem' ); ?>%</p>
                                    </div>
                                    <div class="progress">
                                        <?php 
                                            $value = get_sub_field( 'terraplanagem' ); 
                                            $final = str_replace(',', '.', $value);  
                                        ?>
                                        <div class="progress-bar" role="progressbar" style="width: <?php echo $final;?>%" aria-valuenow="<?php the_sub_field( 'terraplanagem' ); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            <?php endif;?>
                            <?php if(get_sub_field( 'fundacao' )): ?>
                                <div class="col-lg-6">
                                    <div class="row justify-content-between align-items-center m-0">
                                        <p>Fundação </p>
                                        <p><?php the_sub_field( 'fundacao' ); ?>%</p>
                                    </div>
                                    <div class="progress">
                                        <?php 
                                            $value = get_sub_field( 'fundacao' ); 
                                            $final = str_replace(',', '.', $value);  
                                        ?>
                                        <div class="progress-bar" role="progressbar" style="width:  <?php echo $final;?>%" aria-valuenow="<?php the_sub_field( 'fundacao' ); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            <?php endif;?>
                            <?php if(get_sub_field( 'estrutura' )): ?>
                                <div class="col-lg-6">
                                    <div class="row justify-content-between align-items-center m-0">
                                        <p>Estrutura </p>
                                        <p><?php the_sub_field( 'estrutura' ); ?>%</p>
                                    </div>
                                    <div class="progress">
                                        <?php 
                                            $value = get_sub_field( 'estrutura' ); 
                                            $final = str_replace(',', '.', $value);  
                                        ?>
                                        <div class="progress-bar" role="progressbar" style="width: <?php echo $final; ?>%" aria-valuenow="<?php the_sub_field( 'estrtura' ); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            <?php endif;?>
                            <?php if(get_sub_field( 'alvenaria' )): ?>
                                <div class="col-lg-6">
                                    <div class="row justify-content-between align-items-center m-0">
                                        <p>Alvenaria </p>
                                        <p><?php the_sub_field( 'alvenaria' ); ?>%</p>
                                    </div>
                                    <div class="progress">
                                     <?php 
                                            $value = get_sub_field( 'alvenaria' ); 
                                            $final = str_replace(',', '.', $value);  
                                        ?>
                                        <div class="progress-bar" role="progressbar" style="width: <?php echo $final; ?>%" aria-valuenow="<?php the_sub_field( 'alvenaria' ); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            <?php endif;?>

                            <?php if(get_sub_field( 'revestimento' )): ?>
                                <div class="col-lg-6">
                                    <div class="row justify-content-between align-items-center m-0">
                                        <p>Revestimento </p>
                                        <p><?php the_sub_field( 'revestimento' ); ?>%</p>
                                    </div>
                                    <div class="progress">
                                        <?php 
                                            $value = get_sub_field( 'revestimento' ); 
                                            $final = str_replace(',', '.', $value);  
                                        ?>
                                        <div class="progress-bar" role="progressbar" style="width: <?php echo $final; ?>%" aria-valuenow="<?php the_sub_field( 'revestimento' ); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            <?php endif;?>
                            <?php if(get_sub_field( 'esquadrias' )): ?>
                                <div class="col-lg-6">
                                    <div class="row justify-content-between align-items-center m-0">
                                        <p>Esquadrias </p>
                                        <p><?php the_sub_field( 'esquadrias' ); ?>%</p>
                                    </div>
                                    <div class="progress">
                                    <?php 
                                            $value = get_sub_field( 'esquadrias' ); 
                                            $final = str_replace(',', '.', $value);  
                                        ?>
                                        <div class="progress-bar" role="progressbar" style="width: <?php echo $final; ?>%" aria-valuenow="<?php the_sub_field( 'esquadrias' ); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            <?php endif;?>
                            <?php if(get_sub_field( 'instalacoes' )): ?>
                                <div class="col-lg-6">
                                    <div class="row justify-content-between align-items-center m-0">
                                        <p>Instalações </p>
                                        <p><?php the_sub_field( 'instalacoes' ); ?>%</p>
                                    </div>
                                    <div class="progress">
                                    <?php 
                                            $value = get_sub_field( 'instalacoes' ); 
                                            $final = str_replace(',', '.', $value);  
                                        ?>
                                        <div class="progress-bar" role="progressbar" style="width: <?php echo $final; ?>%" aria-valuenow="<?php the_sub_field( 'instalacoes' ); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            <?php endif;?>
                            <?php if(get_sub_field( 'acabamentos' )): ?>
                                <div class="col-lg-6">
                                    <div class="row justify-content-between align-items-center m-0">
                                        <p>Acabamentos </p>
                                        <p><?php the_sub_field( 'acabamentos' ); ?>%</p>
                                    </div>
                                    <div class="progress">
                                    <?php 
                                            $value = get_sub_field( 'acabamentos' ); 
                                            $final = str_replace(',', '.', $value);  
                                        ?>
                                        <div class="progress-bar" role="progressbar" style="width: <?php echo $final; ?>%" aria-valuenow="<?php the_sub_field( 'acabamentos' ); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            <?php endif;?>
                            <?php if(get_sub_field( 'areas_externas' )): ?>
                                <div class="col-lg-6">
                                    <div class="row justify-content-between align-items-center m-0">
                                        <p>Áreas externas </p>
                                        <p><?php the_sub_field( 'areas_externas' ); ?>%</p>
                                    </div>
                                    <div class="progress">
                                    <?php 
                                            $value = get_sub_field( 'areas_externas' ); 
                                            $final = str_replace(',', '.', $value);  
                                        ?>
                                        <div class="progress-bar" role="progressbar" style="width: <?php echo $final; ?>%" aria-valuenow="<?php the_sub_field( 'areas_externas' ); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            <?php endif;?>
                            <?php if(get_sub_field( 'limpeza' )): ?>
                                <div class="col-lg-6">
                                    <div class="row justify-content-between align-items-center m-0">
                                        <p>Limpeza </p>
                                        <p><?php the_sub_field( 'limpeza' ); ?>%</p>
                                    </div>
                                    <div class="progress">
                                    <?php 
                                            $value = get_sub_field( 'limpeza' ); 
                                            $final = str_replace(',', '.', $value);  
                                        ?>
                                        <div class="progress-bar" role="progressbar" style="width: <?php echo $final; ?>%" aria-valuenow="<?php the_sub_field( 'limpeza' ); ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            <?php endif;?>
                            
                        <?php endwhile; ?>
                                            </div>
                                        <?php else : ?>
                                            <?php // no rows found ?>
                                        <?php endif; ?>
                                    </div><!-- /.column-->
                                </div>
                            </div><!-- main row -->
                            <div class="m-0 h-100 carousel_status_mobile align-items-center justify-content-center">
                                <?php $galeria_de_imagens_images = get_field( 'galeria_de_imagens' ); ?>
                                <?php if ( $galeria_de_imagens_images ) :  ?>
                                    <?php foreach ( $galeria_de_imagens_images as $galeria_de_imagens_image ): ?>
                                        <a class="p-0" href="<?php echo esc_url( $galeria_de_imagens_image['url'] ); ?>" data-fancybox="ape" data-caption="<?php echo esc_attr( $galeria_de_imagens_image['alt'] ); ?>">
                                            <img class="col-lg-12 p-0" src="<?php echo esc_url( $galeria_de_imagens_image['sizes']['thumbnail'] ); ?>" alt="<?php echo esc_attr( $galeria_de_imagens_image['alt'] ); ?>" />
                                        </a>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <a class="p-0" href="/wp-content/themes/quartzo/img/shutterstock_667100452.png" data-fancybox="ape" data-caption="Caption #1">
                                        <img class="col-lg-12 p-0" src="/wp-content/themes/quartzo/img/shutterstock_667100452.png" alt="" />
                                    </a>

                                    <a class="p-0" href="/wp-content/themes/quartzo/img/shutterstock_667100452.png" data-fancybox="ape" data-caption="Caption #2">
                                        <img class="col-lg-12 p-0" src="/wp-content/themes/quartzo/img/shutterstock_667100452.png" alt="" />
                                    </a>

                                    <a class="p-0" href="/wp-content/themes/quartzo/img/shutterstock_667100452.png" data-fancybox="ape" data-caption="Caption #1">
                                        <img class="col-lg-12 p-0" src="/wp-content/themes/quartzo/img/shutterstock_667100452.png" alt="" />
                                    </a>

                                    <a class="p-0" href="/wp-content/themes/quartzo/img/shutterstock_667100452.png" data-fancybox="ape" data-caption="Caption #2">
                                        <img class="col-lg-12 p-0" src="/wp-content/themes/quartzo/img/shutterstock_667100452.png" alt="" />
                                    </a>

                                    <a class="p-0" href="/wp-content/themes/quartzo/img/shutterstock_667100452.png" data-fancybox="ape" data-caption="Caption #2">
                                        <img class="col-lg-12 p-0" src="/wp-content/themes/quartzo/img/shutterstock_667100452.png" alt="" />
                                    </a>
                                <?php endif; ?>
                                

                            </div><!-- second row  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- main row -->
    </div>
</section><!-- /.status -->