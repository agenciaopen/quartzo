<?php if(get_field( 'texto_chamativo_titulo' )): ?>
<section class="viver d-none d-lg-block">
    <div class="container h-100 mb-lg-5">
        <div class="row h-100 align-items-center justify-content-start m-0 pb-5">
            <div class=" col-lg-12 row text-left p-0 m-0">
                <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->
                <div class="col-2 col-lg-1 hr-left"></div>
                <h2 class="col-10 col-lg-7 pr-0"><?php the_field( 'texto_chamativo_titulo' ); ?></h2>
            </div><!-- /.col-12 row justify-content-start aling-items-strech -->
            <p class='col-lg-7 p-0 my-3 mb-lg-5'>
            <?php the_field( 'texto_chamativo_descricao' ); ?>            </p>
        </div><!-- /.col-lg-12 text_description -->
    </div>
</section><!-- /.mcmv -->
<?php endif;?>