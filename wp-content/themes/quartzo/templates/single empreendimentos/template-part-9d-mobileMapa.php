<?php if (get_field( 'iframe_mapa' )): ?>

<section class="singleCollapseMapa d-block d-lg-none py-0 mb-3">
    <div class="container h-100 p-0">
        <div class="row h-100 align-items-start justify-content-start m-0">
            <div id="accordion" class="w-100">
                <div class="card">
                    <div class="card-header background_seventh w-100" id="headingMapa">
                        <h5 class="w-100">
                            <button class="d-flex align-items-center justify-content-start btn btn-link background_seventh w-100 p-3" data-toggle="collapse" data-target="#collapseMapa" aria-expanded="true" aria-controls="collapseMapa">
                                <img src='/wp-content/themes/quartzo/img/noun_location pin_3450934.png' class='img-fluid' alt='' title='' lazy='loading'>
                                Localização
                            </button>
                        </h5>
                    </div>
                    <div id="collapseMapa" class="collapse" aria-labelledby="headingMapa" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row m-0 h-100 align-items-center justify-content-center">
                                <div class="col-12 col-lg-8 p-0">
                                    <?php the_field( 'iframe_mapa' ); ?>
                                </div><!-- /.col-9 -->
                                <div class="col-12 col-lg-4">
                                    <ul class="mapa_lista">
                                    <?php if ( have_rows( 'icones_' ) ) : ?>
                                        <?php while ( have_rows( 'icones_' ) ) : the_row(); ?>
                                            <?php if ( get_sub_field( 'imagem_icone' ) ) : ?>
                                                <li class="col-12 item_mapa row align-items-center p-0">
                                                    <div class="col-3 circle_mapa yellow d-flex justify-content-center align-items-center mr-2">
                                                        <img class="img-fluid " src="<?php the_sub_field( 'imagem_icone' ); ?>" alt="">
                                                    </div>
                                                    <p class="col-8 pr-0"> <?php the_sub_field( 'titulo' ); ?></p>
                                                </li>
                                            <?php endif ?>
                                           
                                        <?php endwhile; ?>
                                        <?php else : ?>
                                            <li class="col-12 item_mapa row align-items-center p-0">
                                                <div class="col-3 circle_mapa yellow d-flex justify-content-center align-items-center mr-2">
                                                    <img class="img-fluid " src="/wp-content/themes/quartzo/img/noun_Fitness_2662732 (1).png" alt="">
                                                </div>
                                                <p class="col-8 pr-0">Academia</p>
                                            </li>
                                            <li class="col-12 item_mapa row align-items-center p-0">
                                                <div class="col-3 circle_mapa orange d-flex justify-content-center align-items-center mr-2">
                                                    <img class="img-fluid " src="/wp-content/themes/quartzo/img/noun_market_2663961.png" alt="">
                                                </div>
                                                <p class="col-8">Supermercado</p>
                                            </li>
                                            <li class="col-12 item_mapa row align-items-center p-0">
                                                <div class="col-3 circle_mapa red d-flex justify-content-center align-items-center mr-2">
                                                    <img class="img-fluid " src="/wp-content/themes/quartzo/img/noun_School_1818931.png" alt="">
                                                </div>
                                                <p class="col-8 pr-0">Escola</p>
                                            </li>
                                            <li class="col-12 item_mapa row align-items-center p-0">
                                                <div class="col-3 circle_mapa blue d-flex justify-content-center align-items-center mr-2">
                                                    <img class="img-fluid " src="/wp-content/themes/quartzo/img/noun_bakery_3212544.png" alt="">
                                                </div>
                                                <p class="col-8 pr-0">Padaria</p>
                                            </li>
                                            <li class="col-12 item_mapa row align-items-center p-0">
                                                <div class="col-3 circle_mapa green d-flex justify-content-center align-items-center mr-2">
                                                    <img class="img-fluid " src="/wp-content/themes/quartzo/img/noun_Bank_1985519.png" alt="">
                                                </div>
                                                <p class="col-8 pr-0">Banco</p>
                                            </li>
                                            <li class="col-12 item_mapa row align-items-center p-0">
                                                <div class="col-3 circle_mapa purple d-flex justify-content-center align-items-center mr-2">
                                                    <img class="img-fluid " src="/wp-content/themes/quartzo/img/noun_Restaurant_1071858.png" alt="">
                                                </div>
                                                <p class="col-8 pr-0">Restaurante</p>
                                            </li>
                                            <li class="col-12 item_mapa row align-items-center p-0">
                                                <div class="col-3 circle_mapa lightBlue  d-flex justify-content-center align-items-center mr-2">
                                                    <img class="img-fluid " src="/wp-content/themes/quartzo/img/noun_Lake_2901596.png" alt="">
                                                </div>
                                                <p class="col-8 pr-0">Lagoa da Pampulha</p>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div><!-- /col-3 -->
                            </div><!-- /row -->
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- main row -->
    </div>
</section><!-- /.status -->
<?php endif;?>