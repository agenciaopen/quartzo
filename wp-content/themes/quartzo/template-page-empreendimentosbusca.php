<?php
/**
*
* Template Name: Busca
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

    <?php get_template_part( 'templates/global/template-part', 'barraicones' ); ?>

    <section class="empreendimentos">
    <div class="container h-100">
        <div class="h-100 align-items-center justify-content-center">
            <div class="row m-0 pb-5 col-12 justify-content-start aling-items-strech p-0">
                <!-- <div class="d-none d-lg-inline col-lg-1"></div> -->
                <div class=" col-lg-1 hr-left"></div>
                <h2 class="col-lg-11">empreendimentos - resultado</h2>
            </div><!-- /.col-12 row justify-content-start aling-items-strech -->
            <?php echo do_shortcode('[searchandfilter id="140" show="results"]');?>
        </div><!-- end main row -->
    </div>
</section><!-- /.empreendimentos -->

<?php get_template_part( 'templates/empreendimentos/template-part', '2-portfolio' ); ?>

<?php get_template_part( 'templates/home/template-part', '3-simulacao' ); ?>

<?php get_template_part( 'templates/home/template-part', '4-blog' ); ?>





<?php get_footer(); ?>

             