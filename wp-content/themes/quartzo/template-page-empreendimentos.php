<?php
/**
*
* Template Name: Empreendimentos
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php if(get_field('mostrar_banner', $page_ID)) : ?>
    <?php get_template_part( 'templates/global/template-part', 'banner' ); ?>
<?php else : ?>
    <?php get_template_part( 'templates/global/template-part', 'barraicones' ); ?>

<?php endif;?>
<?php get_template_part( 'templates/empreendimentos/template-part', '1-empreendimentos' ); ?>

<?php get_template_part( 'templates/empreendimentos/template-part', '2-portfolio' ); ?>

<?php get_template_part( 'templates/empreendimentos/template-part', '2a-mobile' ); ?>





<?php get_footer(); ?>

             