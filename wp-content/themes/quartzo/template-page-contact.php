<?php
/**
*
* Template Name: Contato
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<?php get_template_part( 'templates/contact/template-part', '1-fale' ); ?>

<?php get_template_part( 'templates/contact/template-part', '2-motivo' ); ?>

<?php get_template_part( 'templates/contact/template-part', '3-telefone' ); ?>

<?php get_template_part( 'templates/contact/template-part', '4-onde' ); ?>




<?php /*

<?php if ( have_rows( 'cadastro_de_formularios_de_contato' ) ) : ?>
	<?php while ( have_rows( 'cadastro_de_formularios_de_contato' ) ) : the_row(); ?>
		<?php the_sub_field( 'motivo_do_contato' ); ?>
		<?php // The acf_cf7 field type is not supported in this version of the plugin. ?>
		<?php // Contact http://www.hookturn.io to request support for this field type. ?>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
<?php the_field( 'titulo_ja_sou_cliente' ); ?>
<?php if ( get_field( 'icone_ja_sou_cliente' ) ) : ?>
	<img src="<?php the_field( 'icone_ja_sou_cliente' ); ?>" />
<?php endif ?>
<?php the_field( 'subtitulo_ja_sou_cliente' ); ?>
<?php the_field( 'telefone_de_contato_ja_sou_cliente' ); ?>
<?php the_field( 'titulo_whatsapp' ); ?>
<?php if ( get_field( 'icone_whatsapp' ) ) : ?>
	<img src="<?php the_field( 'icone_whatsapp' ); ?>" />
<?php endif ?>
<?php the_field( 'telefone_whatsapp' ); ?>
<?php the_field( 'sede_administrativa_endereco' ); ?>
<?php the_field( 'sede_administrativa_telefone' ); ?>
<?php if ( have_rows( 'cadastro_de_plantoes_de_venda' ) ) : ?>
	<?php while ( have_rows( 'cadastro_de_plantoes_de_venda' ) ) : the_row(); ?>
		<?php the_sub_field( 'endereco' ); ?>
		<?php the_sub_field( 'telefone' ); ?>
	<?php endwhile; ?>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; */?>

<?php get_footer(); ?>