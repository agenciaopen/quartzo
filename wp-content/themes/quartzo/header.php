<!DOCTYPE html>

<html <?php language_attributes(); ?>>



<head>

    <meta charset="<?php bloginfo('charset'); ?>">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="mobile-web-app-capable" content="yes">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">

    <link rel="profile" href="https://gmpg.org/xfn/11">

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <title><?php wp_title(); ?></title>

    

	<link rel='icon' type='image/png' href='/wp-content/themes/quartzo/img/favicon.png'/>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125244626-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-125244626-1');
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-W63XS95');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Ads: 946409058 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-946409058"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'AW-946409058');
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1336643879762737');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1336643879762737&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->


    <?php wp_head(); ?>

    <!-- <link href="https://db.onlinewebfonts.com/c/8b2760b7bcaf4e2cdbb6cacec092b91b?family=MuseoSans-900" rel="stylesheet" type="text/css" /> -->

    <!-- <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" /> -->

    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.1.0/css/hover-min.css" rel="stylesheet" type="text/css" />

<link href="/wp-content/themes/quartzo/css/fonts.css" rel="stylesheet preload" type="text/css" />  -->


</head>





<body <?php body_class(); ?> id="quartzo" >
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W63XS95" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->



    <header>

    <nav class="navbar navbar-expand-xl navbar-default" role="navigation" id="">

        <div class="container-fluid">

           

            <a class="navbar-brand" href="<?php echo home_url(); ?>">

                <img src='<?php the_field('logo_site', 'option') ?>' class='img-fluid' alt='<?php bloginfo( 'name' ); ?>' title='<?php bloginfo( 'name' ); ?>' loading='lazy'>

            </a>

             <!-- Brand and toggle get grouped for better mobile display -->

             <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'your-theme-slug' ); ?>">

                <span class="icon-bar top-bar"></span>

                <span class="icon-bar middle-bar"></span>

                <span class="icon-bar bottom-bar"></span>

            </button>

            <?php

            wp_nav_menu( array(

                'theme_location'    => 'primary',

                'depth'             => 2,

                'container'         => 'div',

                'container_class'   => 'collapse navbar-collapse',

                'container_id'      => 'bs-example-navbar-collapse-1',

                'menu_class'        => 'nav navbar-nav ml-auto align-items-center',

                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',

                'walker'            => new WP_Bootstrap_Navwalker(),

            ) );

            ?>

        </div>

    </nav>



        <nav class="navbar navbar-expand-xl d-none">

            <a class="navbar-brand" href="<?php echo home_url(); ?>">

                <img src="/wp-content/themes/quartzo/img/logo 02.png" width="215" height="52" alt="logo">

            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

                <span class="navbar-toggler-icon">

                    <i class="fa fa-bars"></i>

                </span>

            </button>



            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">

                <ul class="navbar-nav">

                    <li class="nav-item">

                        <a class="nav-link" href="/institucional">Institucional</a>

                    </li>

                    <li class="nav-item">

                        <a class="nav-link" href="/empreendimentos">Empreendimentos</a>

                    </li>

                    <li class="nav-item">

                        <a class="nav-link" href="/minha-casa-minha-vida">MCMV</a>

                    </li>

                    <li class="nav-item">

                        <a class="nav-link" href="#">Blog</a>

                    </li>

                    <li class="nav-item">

                        <a class="nav-link" href="/contato">Contato</a>

                    </li>

                    <li class="nav-item">

                        <a class="nav-link" href="#">Área do cliente</a>

                    </li>

                    <li class="d-flex d-lg-none row col-12 m-0 justify-content-between p-0">

                        <a class="fa_nav" href="#">

                            <i class="fa fa-instagram img-fluid"></i>

                        </a>

                        <a class="fa_nav" href="#">

                            <i class="fa fa-facebook-f"></i>

                        </a>

                        <a class="fa_nav" href="#">

                            <i class="fa fa-youtube"></i>

                        </a>

                        <a class="fa_nav" href="#">

                            <i class="fa fa-linkedin"></i>

                        </a>

                    </li>

                </ul>

            </div>

        </nav>

        <div class="container-fluid search_container">

            <div class="d-none d-xl-flex row col-12 search_nav align-items-center justify-content-center m-0">

                <a href="<?php echo home_url(); ?>">

                    <img class="" src="/wp-content/themes/quartzo/img/logo 03.png" height="52" alt="logo">

                </a>

                <?php echo do_shortcode('[searchandfilter id="140"]') ?>

            </div>

        </div>

        <div class="container search_mobile h-100">

            <div class="d-xl-none row h-100 align-items-center justify-content-center ">

                <div id="accordion" class="accordion col-10">

                    <div class="card search_nav mb-0">

                        <div class="card-header collapsed background_first padding_accordion_about" data-toggle="collapse" href="#collapseNav">

                            <a class="card-title collapse_title">

                                Encontre aqui seu imóvel ideal

                            </a>

                        </div>

                        <div id="collapseNav" class="card-body collapse mb-3" data-parent="#accordion">

                            <?php echo do_shortcode('[searchandfilter id="140"]') ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </header>