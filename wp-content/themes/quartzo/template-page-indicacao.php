<?php

/**
 *
 * Template Name: Indicação
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID

?>

<section class="content pt-0">
    <div class="container-fluid p-0">
        <?php if ( get_field( 'imagem_principal' ) ) : ?>
            <img src='<?php the_field( 'imagem_principal' ); ?>' class='img-fluid d-none d-sm-block' alt='' title='' lazy='loading'>
        <?php endif ?>
        <?php if ( get_field( 'imagem_principal_mobile' ) ) : ?>
            <img src="<?php the_field( 'imagem_principal_mobile' ); ?>" class='img-fluid d-block d-sm-none' alt='' title='' lazy='loading'>
        <?php endif ?>
    </div>
</section>
<section class="content indicacao">
    <div class="container h-100">
        <div class="row h-100 align-items-start justify-content-between">
           <div class="col-md-4 text-center">   
                <img src="<?php the_field( 'icone' ); ?>" alt="" title="" class="img-fluid" />
           </div>
           <div class="col-md-8 text-left">
                <p><?php the_field( 'descricao_' ); ?></p>
                <?php if ( have_rows( 'e_simples_-_cadastro' ) ) : ?>
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            É SIMPLES: 
                        </li>
                        <?php while ( have_rows( 'e_simples_-_cadastro' ) ) : the_row(); ?>
                            <li class="list-inline-item">
                                <svg width="9" height="19" viewBox="0 0 9 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.0705566 18.7888L8.12624 9.53086L0.0705566 0.272949V18.7888Z" fill="#DB3343"/>
                                </svg>
                                <?php the_sub_field( 'texto' ); ?>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
           </div>
        </div>
    </div>
    <div class="container h-100">
        <div class="row h-100 align-items-start justify-content-start">
            <div class="col-md-6 mb-5">
                <h2><?php the_field( 'gostou_da_ideia_titulo' ); ?></h2>
                <p><?php the_field( 'gostou_da_ideia_descricao' ); ?></p>
            </div>
        </div>
        <div class="row h-100 align-items-stretch justify-content-start">
            <?php if ( have_rows( 'cadastro_de_itens' ) ) : ?>
                <?php $count = 1; while ( have_rows( 'cadastro_de_itens' ) ) : the_row(); ?>
                    <div class="col-md-4 text-center item">
                        <?php if ( get_sub_field( 'icone' ) ) : ?>
                            <span class="number d-none">
                                    <?php echo $count;?>.
                                </span>
                            <img src="<?php the_sub_field( 'icone' ); ?>" class="img-fluid" alt="" title="">
                        <?php endif ?>
                        <div class="row h-100 m-0 align-items-start">
                            <div class="col-md-2 d-none d-md-inline">
                                <span class="number">
                                    <?php echo $count;?>.
                                </span>
                            </div>
                            <div class="col-md-10 text-left pl-0">
                                <p><?php the_sub_field( 'descricao', false, false ); ?></p>
                            </div>
                        </div>
                    </div>
                <?php $count++; endwhile; ?>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>                
        </div>
    </div>
</section>
<section class="content dica">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-12 border_bottom">
                <img src='<?php bloginfo('template_url'); ?>/img/icon7.png' class='img-fluid' alt='' title='' lazy='loading'>
                <p><b><?php the_field( 'titulo_dica' ); ?></b></p>
                <p><?php the_field( 'descricao_dica', false, false ); ?></p>
            </div>
            <?php the_field( 'formulario_indicacao2' ); ?>

      
        </div>
    </div>
</section>
<?php get_footer(); ?>

