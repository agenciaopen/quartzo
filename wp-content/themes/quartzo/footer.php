<!-- Footer -->
<footer>
    <section class="sitemap p-0">
        <div class="container-fluid h-100 background_third pt-4 pt-lg-5">
            <div class="row h-100 align-items-center justify-content-center">
                <img class="py-lg-5" src="/wp-content/themes/quartzo/img/logo -2.png" alt="">
                <div class="w-100"></div>
                <div class="col-12 d-none d-lg-flex row justify-content-center">
                    <?php if ( have_rows( 'adicionar_coluna_de_menu', 'option' ) ) : ?>
                        <?php while ( have_rows( 'adicionar_coluna_de_menu', 'option' ) ) : the_row(); ?>
                            <?php $pagina_principal_da_coluna = get_sub_field( 'pagina_principal_da_coluna' ); ?>
                            <ul class="list-unstyled">
                                <?php if ( $pagina_principal_da_coluna ) : ?>
                                    <li>
                                        <a href="<?php echo esc_url( $pagina_principal_da_coluna['url'] ); ?>" target="<?php echo esc_attr( $pagina_principal_da_coluna['target'] ); ?>">
                                            <p><?php echo esc_html( $pagina_principal_da_coluna['title'] ); ?></p>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if ( have_rows( 'cadastro_de_subpaginas' ) ) : ?>
                                    <?php while ( have_rows( 'cadastro_de_subpaginas' ) ) : the_row(); ?>
                                        <?php $subpagina = get_sub_field( 'subpagina' ); ?>
                                        <?php if ( $subpagina ) : ?>
                                            <li>
                                                <a href="<?php echo esc_url( $subpagina['url'] ); ?>" target="<?php echo esc_attr( $subpagina['target'] ); ?>">
                                                    <p><?php echo esc_html( $subpagina['title'] ); ?></p>
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                <?php else : ?>
                                    <?php // no rows found ?>
                                <?php endif; ?>
                            </ul>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>
                
                 
                </div>
            </div><!-- row -->
            <div class="row h-100 align-items-center justify-content-center d-block d-lg-none p-4">
                <div class="col-12 row justify-content-start">
                    <div class="column">
                    <?php if ( have_rows( 'adicionar_coluna_de_menu', 'option' ) ) : ?>
                        <?php while ( have_rows( 'adicionar_coluna_de_menu', 'option' ) ) : the_row(); ?>
                            <?php $pagina_principal_da_coluna = get_sub_field( 'pagina_principal_da_coluna' ); ?>
                                <?php if ( $pagina_principal_da_coluna ) : ?>
                                        <p class="font-weight-bold text-white pb-4">
                                            <a class="font-weight-bold text-white" href="<?php echo esc_url( $pagina_principal_da_coluna['url'] ); ?>" target="<?php echo esc_attr( $pagina_principal_da_coluna['target'] ); ?>">
                                                <?php echo esc_html( $pagina_principal_da_coluna['title'] ); ?>
                                            </a>                            
                                        </p>
                                <?php endif; ?>
                                <?php if ( have_rows( 'cadastro_de_subpaginas' ) ) : ?>
                                    <?php while ( have_rows( 'cadastro_de_subpaginas' ) ) : the_row(); ?>
                                        <?php $subpagina = get_sub_field( 'subpagina' ); ?>
                                        <?php if ( $subpagina ) : ?>
                                            <p class="font-weight-bold text-white pb-4">
                                                <a class="font-weight-bold text-white" href="<?php echo esc_url( $subpagina['url'] ); ?>" target="<?php echo esc_attr( $subpagina['target'] ); ?>">
                                                    <?php echo esc_html( $subpagina['title'] ); ?>
                                                </a>
                                            </p>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                <?php else : ?>
                                    <?php // no rows found ?>
                                <?php endif; ?>
                            </ul>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>
                       
                    </div>
                </div>
            </div>
        </div><!-- container -->
        <div class="container-fluid background_fifth p-3">
            <div class="row m-0 justify-content-center  ">
                <div class="col-12 col-lg-5">
                    <div class="row justify-content-center m-0">
                        <?php if ( have_rows( 'cadastro_de_redes_sociais', 'option' ) ) : ?>
                            <?php while ( have_rows( 'cadastro_de_redes_sociais', 'option' ) ) : the_row(); ?>
                                <a href="<?php the_sub_field( 'link' ); ?>" target="_blank" class="mr-3 mr-md-5">
                                    <?php the_sub_field( 'icone' ); ?>
                                </a>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found ?>
                        <?php endif; ?>
                 
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.sitemap -->
</footer>


<?php wp_footer(); ?>
<?php if ( wp_is_mobile() ) : ?>
    /* Display and echo mobile specific stuff here */
<?php else : ?>
   <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<?php endif; ?>

<script src="https://unpkg.com/blip-chat-widget" type="text/javascript">

</script>

<script>

    (function () {
        $(document).on('click', '#chatblip', function(event) { 
            event.preventDefault(); 
            $("#blip-chat-icon").click(); 
            $("#blip-chat-container").addClass('d-block'); 

        });
		    $(document).on('click', '#blip-chat-close-icon', function(event) { 
            event.preventDefault(); 
            $("#blip-chat-container").removeClass('d-block'); 
        });
        window.onload = function () {

            new BlipChat()

            .withAppKey('cXVhcnR6bzI6NjBmMjk0NzMtODZlZi00MTMzLTkxZGMtYjE5ZTM0MWFhNWRl')

            .withButton({"color":"#2CC3D5","icon":""})

            .withCustomCommonUrl('https://chat.blip.ai/')

            .build();

        }

    })();

</script>
<script type='text/javascript' async src='https://d335luupugsy2.cloudfront.net/js/loader-scripts/2306a0e2-6677-4e97-89a1-d20f15afb0ab-loader.js'></script>
  <script>
    AOS.init({disable: 'mobile'});

  </script>
</body>

</html>